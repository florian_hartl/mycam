﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Security.Cryptography;
using Logimove_Dll;
using System.IO;


namespace MyCamNetRemotingLib
{
    class MD5String
    {
        public static string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }

    public class UserManager
    {
        private static Dictionary<string, string> usersList = new Dictionary<string, string>();
        //private static Dictionary<string, string> usersSessions = new Dictionary<string, string>();        
        private static List<string> userSessions = new List<string>();


        private static string lastUsersFile = "";

        public static int loadUsersList(string filePath)
        {
            usersList.Clear();

            string[] lines = File.ReadAllLines(filePath);
            usersList = lines.Select(l => l.Split('=')).ToDictionary(a => a[0], a => a[1]);

            lastUsersFile = filePath;
            return lines.Length;
        }

        public static bool saveUsersList()
        {
            if (lastUsersFile.Length > 0)
            {
                string[] lines = usersList.Select(kvp => kvp.Key + "=" + kvp.Value).ToArray();
                File.WriteAllLines(lastUsersFile, lines);

                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool changeUserPassword(string user, string oldPasswd, string newPasswd)
        {
            if (checkUserWithPasswd(user, oldPasswd))
            {
                usersList[user] = newPasswd;
                if (saveUsersList())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            } else { 
                return false;
            }
        }

        public static string userLogin(string user, string passwd) 
        {
            if (checkUserWithPasswd(user, passwd))
            {
                Int32 unixTimestamp = (Int32)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                string session = MD5String.CalculateMD5Hash(String.Format("{0}_{1}_{2}", user, passwd, unixTimestamp));
                userSessions.Add(session);

                return session;
            }
            else 
            {
                return "FAIL";            
            }
        }

        public static bool checkSession(string session) {
            int index = userSessions.IndexOf(session);

            if (index != -1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool userLogout(string session)
        {
            userSessions.Remove(session);
            return true;
        }

        private static bool checkUserWithPasswd(string user, string passwd)
        {
            if (usersList.ContainsKey(user) && usersList[user] == passwd) {
                return true;
            } else {
                return false;
            }
        }
    }

    public class MyCamRemotingObject : MarshalByRefObject
    {

        public string user(string name, string password)
        {
            return UserManager.userLogin(name, password);
        }

        public bool exit(string session)
        {
            return UserManager.userLogout(session);
        }

        public bool movetos(string session, string direction)
        {
            if (UserManager.checkSession(session))
            {
                QuickCam.movetos(direction);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool moveto(string session, int x, int y)
        {
            if (UserManager.checkSession(session))
            {
                QuickCam.moveto(x, y);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool observe(string session)
        {
            if (UserManager.checkSession(session))
            {
                QuickCam.observe();
                return true;
            }
            else
            {
                return false;
            }
        }

        public string snap(string session, string fileName)
        {
            if (UserManager.checkSession(session))
            {
                return QuickCam.snap(fileName); ;
            }
            else
            {
                return "";
            }
        }

        public bool passwd(string session, string user, string oldPasswd, string newPasswd)
        {
            if (UserManager.checkSession(session))
            {
                if (UserManager.changeUserPassword(user, oldPasswd, newPasswd))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
