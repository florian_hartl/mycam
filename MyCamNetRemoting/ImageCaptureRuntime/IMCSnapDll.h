/************************************************************/
/*                                                          */
/* Inhalt:    Schnittstellen f�r einfache Bildaufnahme      */
/*                                                          */
/* Autor(en): Josef P�sl (jp), <XXX>                        */
/* Firma:     Fachhochschule Amberg-Weiden                  */
/* Stand:     19. Mai 2006                                  */
/*                                                          */
/* Historie:  19. Mai 2006 jp  erstellt                     */
/*            xx. xxx xxxx     modifiziert...               */
/*                                                          */
/* Copyright 2001-2050 FH Amberg-Weiden ... usw.            */
/*                                                          */
/************************************************************/

#ifndef IMCSNAPDLL_H
#define IMCSNAPDLL_H

#include <windows.h>

#ifdef IMCSNAPDLL_EXPORTS
#define IMCSNAPDLL_API __declspec(dllexport)
#else
#define IMCSNAPDLL_API __declspec(dllimport)
#endif


#ifdef __cplusplus
extern "C" {
#endif

// module init and exit
IMCSNAPDLL_API HRESULT IMCSnapInit(void);
IMCSNAPDLL_API HRESULT IMCSnapExit(void);

// device info functions
IMCSNAPDLL_API HRESULT IMCSnapGetDeviceCount(int *devCount);
IMCSNAPDLL_API HRESULT IMCSnapGetDeviceName(int devInd, char *devName, int *devNameMaxLen);

// worker function(s): take a picture
IMCSNAPDLL_API HRESULT IMCSnapSnap(int devInd, char *filePath);

#ifdef __cplusplus
}
#endif

#endif  // IMCSNAPDLL_H