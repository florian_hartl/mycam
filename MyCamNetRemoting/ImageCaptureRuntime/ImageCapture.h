/************************************************************/
/*                                                          */
/* Inhalt:    Schnittstellen f�r Bildaufnahme(-ger�te)      */
/*                                                          */
/* Autor(en): Josef P�sl (jp), <XXX>                        */
/* Firma:     Fachhochschule Amberg-Weiden                  */
/* Stand:     06. Jan 2005                                  */
/*                                                          */
/* Historie:  06. Jan 2005 jp  erstellt                     */
/*            xx. xxx xxxx     modifiziert...               */
/*                                                          */
/* Copyright 2001-2050 FH Amberg-Weiden ... usw.            */
/*                                                          */
/************************************************************/

#ifndef IMAGE_CAPTURE_H
#define IMAGE_CAPTURE_H

#include <unknwn.h>

#define IMCAPI STDMETHODCALLTYPE


class IImage : public IUnknown
{
public:
	virtual HRESULT IMCAPI GetBitmapInfo(BITMAPINFOHEADER *bmiHeader) = 0;
	virtual HRESULT IMCAPI GetBits(long *bufferSize, long *buffer) = 0;
	virtual HRESULT IMCAPI CreateHBitmap(HBITMAP *hBmp) = 0;
	virtual HRESULT IMCAPI Save(BSTR filePath) = 0;
};

class IGrabDevice : public IUnknown
{
public:
	virtual HRESULT IMCAPI GetName(BSTR *name) = 0;
	virtual HRESULT IMCAPI GrabImage(IImage **img) = 0;
};

class IEnumGrabDevices : public IUnknown
{
public:
	virtual HRESULT IMCAPI Next( 
						ULONG         gdNumToFetch,
						IGrabDevice **gdArr,
						ULONG        *gdNumFetched) = 0;
	virtual HRESULT IMCAPI Skip(ULONG gdNum) = 0;
	virtual HRESULT IMCAPI Reset(void) = 0;
	virtual HRESULT IMCAPI Clone(IEnumGrabDevices **enumNew) = 0;
};

#ifdef IMAGE_CAPTURE_INTERNAL
HRESULT IMCAPI CreateEnumGrabDevices(IEnumGrabDevices **ppenum);
#endif

#endif // IMAGE_CAPTURE_H
