﻿using MyCamNetRemotingLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace MyCamNetRemotingClient
{
    public partial class Form1 : Form
    {
        private TcpChannel channel;
        private MyCamRemotingObject remoteObject;

        private string userSession;

        public Form1()
        {
            InitializeComponent();
        }

        private void button_Connect_Click(object sender, EventArgs e)
        {
            string serverPath = textBox_ServerPath.Text;

            channel = new TcpChannel();
            ChannelServices.RegisterChannel(channel, false);

            remoteObject = (MyCamRemotingObject)Activator.GetObject(
                typeof(MyCamRemotingObject),
                serverPath);

            button_Connect.Enabled = false;
            button_Disconnect.Enabled = true;
            button_Login.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label1.Text = (remoteObject.moveto(" ", 1, 1)).ToString();
        }

        private void button_Login_Click(object sender, EventArgs e)
        {
            string userName = textBox_UserName.Text;
            string userPassword = textBox_UserPassword.Text;

            userSession = remoteObject.user(userName, userPassword);
            if (userSession != "FAIL")
            {
                this.Text = "User session: " + userSession;

                button_Login.Enabled = false;
                button_Logout.Enabled = true;
                button_ChangePass.Enabled = true;

                button_MoveD.Enabled = true;
                button_MoveC.Enabled = true;
                button_MoveU.Enabled = true;
                button_MoveR.Enabled = true;
                button_MoveL.Enabled = true;
                button_MoveLD.Enabled = true;
                button_MoveLU.Enabled = true;
                button_MoveRD.Enabled = true;
                button_MoveRU.Enabled = true;
                button_Observe.Enabled = true;
                button_Snap.Enabled = true;
            }
            else
            {
                this.Text = "Authorization faild";
            }
        }

        private void button_MoveLD_Click(object sender, EventArgs e)
        {
            try
            {
                remoteObject.movetos(userSession, "lb");
            }
            catch
            {
                this.Text = "Remoting error. Try to reconnect";
            }
        }

        private void button_MoveRD_Click(object sender, EventArgs e)
        {
            try
            {
                remoteObject.movetos(userSession, "rb");
            }
            catch
            {
                this.Text = "Remoting error. Try to reconnect";
            }
        }

        private void button_MoveC_Click(object sender, EventArgs e)
        {
            try
            {
                remoteObject.movetos(userSession, "center");
            }
            catch
            {
                this.Text = "Remoting error. Try to reconnect";
            }
        }

        private void button_MoveLU_Click(object sender, EventArgs e)
        {
            try
            {
                remoteObject.movetos(userSession, "lt");
            }
            catch
            {
                this.Text = "Remoting error. Try to reconnect";
            }
        }

        private void button_MoveRU_Click(object sender, EventArgs e)
        {
            try
            {
                remoteObject.movetos(userSession, "rt");
            }
            catch
            {
                this.Text = "Remoting error. Try to reconnect";
            }
        }

        private void button_MoveU_Click(object sender, EventArgs e)
        {
            try
            {
                remoteObject.moveto(userSession, 0, 1);
            }
            catch
            {
                this.Text = "Remoting error. Try to reconnect";
            }
        }

        private void button_MoveL_Click(object sender, EventArgs e)
        {
            try
            {
                remoteObject.moveto(userSession, -1, 0);
            }
            catch
            {
                this.Text = "Remoting error. Try to reconnect";
            }
        }

        private void button_MoveR_Click(object sender, EventArgs e)
        {
            try
            {
                remoteObject.moveto(userSession, 1, 0);
            }
            catch
            {
                this.Text = "Remoting error. Try to reconnect";
            }
        }

        private void button_MoveD_Click(object sender, EventArgs e)
        {
            try
            {
                remoteObject.moveto(userSession, 0, -1);
            }
            catch
            {
                this.Text = "Remoting error. Try to reconnect";
            }
        }

        private void button_Logout_Click(object sender, EventArgs e)
        {
            try
            {
                if (remoteObject.exit(userSession))
                {
                    userSession = "";

                    button_Login.Enabled = true;
                    button_Logout.Enabled = false;
                    button_ChangePass.Enabled = false;

                    button_MoveD.Enabled = false;
                    button_MoveC.Enabled = false;
                    button_MoveU.Enabled = false;
                    button_MoveR.Enabled = false;
                    button_MoveL.Enabled = false;
                    button_MoveLD.Enabled = false;
                    button_MoveLU.Enabled = false;
                    button_MoveRD.Enabled = false;
                    button_MoveRU.Enabled = false;
                    button_Observe.Enabled = false;
                    button_Snap.Enabled = false;
                }
            }
            catch
            {
                this.Text = "Remoting error. Try to reconnect";
            }
        }

        private void button_Observe_Click(object sender, EventArgs e)
        {
            try
            {
                remoteObject.observe(userSession);
            }
            catch
            {
                this.Text = "Remoting error. Try to reconnect";
            }
        }

        private void button_Snap_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 unixTimestamp = (Int32)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                string fileName = unixTimestamp.ToString() + ".bmp";
                
                string encodedData = remoteObject.snap(userSession, fileName);
                if (encodedData.Length > 0) {
  
                    string path = AppDomain.CurrentDomain.BaseDirectory + "images\\";
                    if (!(Directory.Exists(path)))
                    {
                        Directory.CreateDirectory(path);
                    }

                    byte[] filebytes = Convert.FromBase64String(encodedData);
                    FileStream fs = new FileStream(path + fileName, FileMode.CreateNew, FileAccess.Write, FileShare.None);
                    fs.Write(filebytes, 0, filebytes.Length);
                    fs.Close();

                    pictureBox.Image = new Bitmap(path + fileName);
                }

            }
            catch
            {
                this.Text = "Remoting error. Try to reconnect";
            }
        }

        private void button_Disconnect_Click(object sender, EventArgs e)
        {
            ChannelServices.UnregisterChannel(channel);

            userSession = "";

            button_Connect.Enabled = true;
            button_Disconnect.Enabled = false;

            button_Login.Enabled = false;
            button_Logout.Enabled = false;
            button_ChangePass.Enabled = false;

            button_MoveD.Enabled = false;
            button_MoveC.Enabled = false;
            button_MoveU.Enabled = false;
            button_MoveR.Enabled = false;
            button_MoveL.Enabled = false;
            button_MoveLD.Enabled = false;
            button_MoveLU.Enabled = false;
            button_MoveRD.Enabled = false;
            button_MoveRU.Enabled = false;
            button_Observe.Enabled = false;
            button_Snap.Enabled = false;
        }

        private void button_ChangePass_Click(object sender, EventArgs e)
        {
            string userName = textBox_UserName.Text;
            string userPassword = textBox_UserPassword.Text;
            string newPassword = textBox_NewPassword.Text;
            
            try
            {
                if (remoteObject.passwd(userSession, userName, userPassword, newPassword))
                {
                    this.Text = "Password changed";
                    textBox_NewPassword.Text = "";
                } else {
                    this.Text = "Change password error.";
                }
            }
            catch
            {
                this.Text = "Remoting error. Try to reconnect";
            }
        }
    }
}
