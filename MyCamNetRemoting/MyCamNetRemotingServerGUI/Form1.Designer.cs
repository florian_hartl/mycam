﻿namespace MyCamNetRemotingServerGUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button_StopService = new System.Windows.Forms.Button();
            this.button_StartService = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_ServiceName = new System.Windows.Forms.TextBox();
            this.textBox_ServicePort = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button_SelectCamera = new System.Windows.Forms.Button();
            this.button_UpdateCameraList = new System.Windows.Forms.Button();
            this.comboBox_CameraList = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button_LoadUsers = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.openFileDialog_Users = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button_StopService);
            this.groupBox1.Controls.Add(this.button_StartService);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBox_ServiceName);
            this.groupBox1.Controls.Add(this.textBox_ServicePort);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(552, 94);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Service Settings";
            // 
            // button_StopService
            // 
            this.button_StopService.Enabled = false;
            this.button_StopService.Location = new System.Drawing.Point(457, 33);
            this.button_StopService.Name = "button_StopService";
            this.button_StopService.Size = new System.Drawing.Size(75, 31);
            this.button_StopService.TabIndex = 5;
            this.button_StopService.Text = "Stop";
            this.button_StopService.UseVisualStyleBackColor = true;
            this.button_StopService.Click += new System.EventHandler(this.button_StopService_Click);
            // 
            // button_StartService
            // 
            this.button_StartService.Location = new System.Drawing.Point(360, 32);
            this.button_StartService.Name = "button_StartService";
            this.button_StartService.Size = new System.Drawing.Size(75, 32);
            this.button_StartService.TabIndex = 4;
            this.button_StartService.Text = "Start";
            this.button_StartService.UseVisualStyleBackColor = true;
            this.button_StartService.Click += new System.EventHandler(this.button_StartService_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(176, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Port";
            // 
            // textBox_ServiceName
            // 
            this.textBox_ServiceName.Location = new System.Drawing.Point(227, 37);
            this.textBox_ServiceName.Name = "textBox_ServiceName";
            this.textBox_ServiceName.Size = new System.Drawing.Size(100, 22);
            this.textBox_ServiceName.TabIndex = 1;
            this.textBox_ServiceName.Text = "LogiCam";
            // 
            // textBox_ServicePort
            // 
            this.textBox_ServicePort.Location = new System.Drawing.Point(57, 37);
            this.textBox_ServicePort.Name = "textBox_ServicePort";
            this.textBox_ServicePort.Size = new System.Drawing.Size(100, 22);
            this.textBox_ServicePort.TabIndex = 0;
            this.textBox_ServicePort.Text = "8080";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button_SelectCamera);
            this.groupBox2.Controls.Add(this.button_UpdateCameraList);
            this.groupBox2.Controls.Add(this.comboBox_CameraList);
            this.groupBox2.Location = new System.Drawing.Point(12, 112);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(552, 87);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Camera Settings";
            // 
            // button_SelectCamera
            // 
            this.button_SelectCamera.Location = new System.Drawing.Point(457, 26);
            this.button_SelectCamera.Name = "button_SelectCamera";
            this.button_SelectCamera.Size = new System.Drawing.Size(75, 32);
            this.button_SelectCamera.TabIndex = 7;
            this.button_SelectCamera.Text = "Activate";
            this.button_SelectCamera.UseVisualStyleBackColor = true;
            this.button_SelectCamera.Click += new System.EventHandler(this.button_SelectCamera_Click);
            // 
            // button_UpdateCameraList
            // 
            this.button_UpdateCameraList.Location = new System.Drawing.Point(346, 26);
            this.button_UpdateCameraList.Name = "button_UpdateCameraList";
            this.button_UpdateCameraList.Size = new System.Drawing.Size(89, 32);
            this.button_UpdateCameraList.TabIndex = 6;
            this.button_UpdateCameraList.Text = "Update list";
            this.button_UpdateCameraList.UseVisualStyleBackColor = true;
            this.button_UpdateCameraList.Click += new System.EventHandler(this.button_UpdateCameraList_Click);
            // 
            // comboBox_CameraList
            // 
            this.comboBox_CameraList.FormattingEnabled = true;
            this.comboBox_CameraList.Location = new System.Drawing.Point(20, 31);
            this.comboBox_CameraList.Name = "comboBox_CameraList";
            this.comboBox_CameraList.Size = new System.Drawing.Size(307, 24);
            this.comboBox_CameraList.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.button_LoadUsers);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Location = new System.Drawing.Point(13, 206);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(551, 99);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "User Settings";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(345, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 17);
            this.label5.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(127, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 17);
            this.label4.TabIndex = 9;
            // 
            // button_LoadUsers
            // 
            this.button_LoadUsers.Location = new System.Drawing.Point(19, 43);
            this.button_LoadUsers.Name = "button_LoadUsers";
            this.button_LoadUsers.Size = new System.Drawing.Size(89, 32);
            this.button_LoadUsers.TabIndex = 8;
            this.button_LoadUsers.Text = "Open File";
            this.button_LoadUsers.UseVisualStyleBackColor = true;
            this.button_LoadUsers.Click += new System.EventHandler(this.button_LoadUsers_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(127, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(139, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Passwords File Path:";
            // 
            // openFileDialog_Users
            // 
            this.openFileDialog_Users.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 325);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button_StartService;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_ServiceName;
        private System.Windows.Forms.TextBox textBox_ServicePort;
        private System.Windows.Forms.Button button_StopService;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button_SelectCamera;
        private System.Windows.Forms.Button button_UpdateCameraList;
        private System.Windows.Forms.ComboBox comboBox_CameraList;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button_LoadUsers;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.OpenFileDialog openFileDialog_Users;
        private System.Windows.Forms.Label label5;
    }
}

