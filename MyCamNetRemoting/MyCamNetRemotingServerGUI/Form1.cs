﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using MyCamNetRemotingLib;
using Logimove_Dll;

namespace MyCamNetRemotingServerGUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private TcpChannel channel;
        private bool serviceRegistered = false;

        private void button_StartService_Click(object sender, EventArgs e)
        {
            string serviceName = textBox_ServiceName.Text;
            int servicePort = Convert.ToInt32(textBox_ServicePort.Text);

            // Create an instance of a channel
            channel = new TcpChannel(servicePort);
            ChannelServices.RegisterChannel(channel, false);

            // Register as an available service with the name
            RemotingConfiguration.RegisterWellKnownServiceType(
                typeof(MyCamRemotingObject),
                serviceName,
                WellKnownObjectMode.Singleton);

            serviceRegistered = true;
            this.Text = String.Format("Service registered with name \"{0}\" at port {1}", serviceName, servicePort);
            button_StartService.Enabled = false;
            button_StopService.Enabled = true;
        }

        private void button_StopService_Click(object sender, EventArgs e)
        {
            
            ChannelServices.UnregisterChannel(channel);

            serviceRegistered = false;
            this.Text = String.Format("Service not registered");
            button_StartService.Enabled = true;
            button_StopService.Enabled = false;
        }

        private void button_UpdateCameraList_Click(object sender, EventArgs e)
        {
            comboBox_CameraList.Items.Clear();
            
            string[] cams = QuickCam.getCams();

            for (int i = 0; i < cams.Length; ++i)
            {
                comboBox_CameraList.Items.Add(String.Format("[{0}] - {1}", i + 1, cams[i]));
            }
        }

        private void button_LoadUsers_Click(object sender, EventArgs e)
        {
            openFileDialog_Users.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;
            openFileDialog_Users.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            //openFileDialog_Users.restoredirectory = true;

            if(openFileDialog_Users.ShowDialog() == DialogResult.OK)
            {
                int usersCount = UserManager.loadUsersList(openFileDialog_Users.FileName);
                label4.Text = openFileDialog_Users.FileName;
                label5.Text = String.Format("Total users: {0}", usersCount);
            }
        }

        private void button_SelectCamera_Click(object sender, EventArgs e)
        {
            int camera = 0;
            camera = comboBox_CameraList.SelectedIndex;
            if (camera != -1)
            {
                QuickCam.selectCamera(camera+1);
            }
            else
            {
                this.Text = "No camera available";
            }
        }
    }
}
