﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.IO;

namespace Logimove_Dll
{
    

    public class QuickCam
    {
        #region DLL-exports

        [DllImport("Logimove_Dll.dll", CallingConvention=CallingConvention.Cdecl, EntryPoint = "?LGM_GetDeviceCount@@YAJPAH@Z")]
        private extern static int LGM_GetDeviceCount(ref int connectedDevices);

        [DllImport("Logimove_Dll.dll", CallingConvention = CallingConvention.Cdecl, EntryPoint = "?LGM_GetDevicePath@@YAJHPADPAH@Z")]
        private extern static int LGM_GetDevicePath(int devInd, StringBuilder devPath, ref int devPathMaxLen);

        [DllImport("Logimove_Dll.dll", CallingConvention = CallingConvention.Cdecl, EntryPoint = "?LGM_GetDeviceName@@YAJHPADPAH@Z")]
        private extern static int LGM_GetDeviceName(int devInd, StringBuilder devName, ref int devNameMaxLen);

        [DllImport("Logimove_Dll.dll", CallingConvention = CallingConvention.Cdecl, EntryPoint = "?LGM_OpenDevice@@YAJHPAPAX@Z")]
        private extern static int LGM_OpenDevice(int devInd, ref int devHandle);

        [DllImport("Logimove_Dll.dll", CallingConvention = CallingConvention.Cdecl, EntryPoint = "?LGM_CloseHandle@@YAJPAPAX@Z")]
        private extern static int LGM_CloseHandle(ref int devHandle);

        [DllImport("Logimove_Dll.dll", CallingConvention = CallingConvention.Cdecl, EntryPoint = "?LGM_MoveHome@@YAJPAX@Z")]
        private extern static int LGM_MoveHome(int devHandle);

        [DllImport("Logimove_Dll.dll", CallingConvention = CallingConvention.Cdecl, EntryPoint = "?LGM_MoveLeft@@YAJPAX@Z")]
        private extern static int LGM_MoveLeft(int devHandle);

        [DllImport("Logimove_Dll.dll", CallingConvention = CallingConvention.Cdecl, EntryPoint = "?LGM_MoveRight@@YAJPAX@Z")]
        private extern static int LGM_MoveRight(int devHandle);

        [DllImport("Logimove_Dll.dll", CallingConvention = CallingConvention.Cdecl, EntryPoint = "?LGM_MoveUp@@YAJPAX@Z")]
        private extern static int LGM_MoveUp(int devHandle);

        [DllImport("Logimove_Dll.dll", CallingConvention = CallingConvention.Cdecl, EntryPoint = "?LGM_MoveDown@@YAJPAX@Z")]
        private extern static int LGM_MoveDown(int devHandle);

        [DllImport("Logimove_Dll.dll", CallingConvention = CallingConvention.Cdecl, EntryPoint = "?LGM_MoveCompleteLeft@@YAJPAX@Z")]
        private extern static int LGM_MoveCompleteLeft(int devHandle);

        [DllImport("Logimove_Dll.dll", CallingConvention = CallingConvention.Cdecl, EntryPoint = "?LGM_MoveCompleteRight@@YAJPAX@Z")]
        private extern static int LGM_MoveCompleteRight(int devHandle);

        [DllImport("Logimove_Dll.dll", CallingConvention = CallingConvention.Cdecl, EntryPoint = "?LGM_MoveCompleteUp@@YAJPAX@Z")]
        private extern static int LGM_MoveCompleteUp(int devHandle);

        [DllImport("Logimove_Dll.dll", CallingConvention = CallingConvention.Cdecl, EntryPoint = "?LGM_MoveCompleteDown@@YAJPAX@Z")]
        private extern static int LGM_MoveCompleteDown(int devHandle);

        #endregion

        private static int devHandle = 0;
        public static bool isReady = false;
        

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static string[] getCams() {
            int count = 0;
            LGM_GetDeviceCount(ref count);

            string[] cameras = new string[count];

            for (int i = 0; i < count; ++i)
            {
                int cameraNameLength = 0;
                LGM_GetDeviceName(i+1, new StringBuilder(), ref cameraNameLength);

                StringBuilder cameraName = new StringBuilder(cameraNameLength+1);
                LGM_GetDeviceName(i + 1, cameraName, ref cameraNameLength);
                cameras[i] = cameraName.ToString();
            }
            return cameras;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static bool selectCamera(int camera) {
            int count = 0;
            LGM_GetDeviceCount(ref count);

            if (camera <= count && camera > 0)
            {
                if (devHandle > 0)
                    LGM_CloseHandle(ref devHandle);

                LGM_OpenDevice(camera, ref devHandle);

                isReady = true;
                return true;
            }

            return false;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static bool releaseCamera(int camera)
        {
            LGM_CloseHandle(ref devHandle);
            devHandle = 0;

            isReady = false;
            return true;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static int moveto(int x, int y)
        {
            int count = 0;
            LGM_GetDeviceCount(ref count);
            
            if (devHandle > 0 && count > 0)
            {
                System.Threading.Thread.Sleep(75);

                // Move camera left/right
                if (x > 0)
                {
                    LGM_MoveRight(devHandle);
                }
                else if (x < 0)
                {
                    LGM_MoveLeft(devHandle);
                }

                System.Threading.Thread.Sleep(75);

                //Move camera up/down
                if (y < 0)
                {
                    LGM_MoveUp(devHandle);
                }
                else if (y > 0)
                {
                    LGM_MoveDown(devHandle);
                }
            }
            return 1;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static int movetos(string direction)
        {
            System.Threading.Thread.Sleep(1000);
            switch (direction)
            {
                case "lb":
                    Console.WriteLine("Move to lb");

                    LGM_MoveCompleteUp(devHandle);
                    System.Threading.Thread.Sleep(1000);
                    LGM_MoveCompleteLeft(devHandle);
                    break;
                case "lt":
                    Console.WriteLine("Move to lt");

                    LGM_MoveCompleteDown(devHandle);
                    System.Threading.Thread.Sleep(1000);
                    LGM_MoveCompleteLeft(devHandle);
                    break;
                case "rb":
                    Console.WriteLine("Move to rb");

                    LGM_MoveCompleteUp(devHandle);
                    System.Threading.Thread.Sleep(1000);
                    LGM_MoveCompleteRight(devHandle);
                    break;
                case "rt":
                    Console.WriteLine("Move to rt");

                    LGM_MoveCompleteDown(devHandle);
                    System.Threading.Thread.Sleep(1000);
                    LGM_MoveCompleteRight(devHandle);;
                    break;
                case "center":
                    Console.WriteLine("Move to center");

                    LGM_MoveHome(devHandle);
                    break;
            }
            return 1;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static bool observe() {
            System.Threading.Thread.Sleep(1000);
            LGM_MoveCompleteRight(devHandle);
            System.Threading.Thread.Sleep(1000);
            LGM_MoveCompleteDown(devHandle);
            System.Threading.Thread.Sleep(1000);
            LGM_MoveCompleteLeft(devHandle);
            System.Threading.Thread.Sleep(1000);
            LGM_MoveCompleteUp(devHandle);
            System.Threading.Thread.Sleep(1000);
            LGM_MoveHome(devHandle);
            return true;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static bool snap(string fileName)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "images\\";
            if (!(Directory.Exists(path)))
            {
                Directory.CreateDirectory(path);
            }

            Console.WriteLine(path + fileName);

            return true;
        }


    }
}
