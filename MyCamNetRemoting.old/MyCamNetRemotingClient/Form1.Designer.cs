﻿namespace MyCamNetRemotingClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_Connect = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBox_UserName = new System.Windows.Forms.TextBox();
            this.textBox_UserPassword = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button_Disconnect = new System.Windows.Forms.Button();
            this.button_MoveL = new System.Windows.Forms.Button();
            this.button_MoveR = new System.Windows.Forms.Button();
            this.button_MoveU = new System.Windows.Forms.Button();
            this.button_MoveD = new System.Windows.Forms.Button();
            this.button_MoveC = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_ServerPath = new System.Windows.Forms.TextBox();
            this.button_Login = new System.Windows.Forms.Button();
            this.button_Logout = new System.Windows.Forms.Button();
            this.button_MoveLU = new System.Windows.Forms.Button();
            this.button_MoveRU = new System.Windows.Forms.Button();
            this.button_MoveLD = new System.Windows.Forms.Button();
            this.button_MoveRD = new System.Windows.Forms.Button();
            this.button_Observe = new System.Windows.Forms.Button();
            this.button_Snap = new System.Windows.Forms.Button();
            this.button_ChangePass = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_NewPassword = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button_Connect
            // 
            this.button_Connect.Location = new System.Drawing.Point(900, 40);
            this.button_Connect.Name = "button_Connect";
            this.button_Connect.Size = new System.Drawing.Size(99, 30);
            this.button_Connect.TabIndex = 0;
            this.button_Connect.Text = "Connect";
            this.button_Connect.UseVisualStyleBackColor = true;
            this.button_Connect.Click += new System.EventHandler(this.button_Connect_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(841, 576);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "TestRequest";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(922, 582);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "label1";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::MyCamNetRemotingClient.Properties.Resources.splash_screen;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(800, 600);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // textBox_UserName
            // 
            this.textBox_UserName.Location = new System.Drawing.Point(900, 117);
            this.textBox_UserName.Name = "textBox_UserName";
            this.textBox_UserName.Size = new System.Drawing.Size(100, 22);
            this.textBox_UserName.TabIndex = 4;
            this.textBox_UserName.Text = "guest";
            // 
            // textBox_UserPassword
            // 
            this.textBox_UserPassword.Location = new System.Drawing.Point(900, 161);
            this.textBox_UserPassword.Name = "textBox_UserPassword";
            this.textBox_UserPassword.PasswordChar = '*';
            this.textBox_UserPassword.Size = new System.Drawing.Size(100, 22);
            this.textBox_UserPassword.TabIndex = 5;
            this.textBox_UserPassword.Text = "guest";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(856, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "User";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(825, 164);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Password";
            // 
            // button_Disconnect
            // 
            this.button_Disconnect.Enabled = false;
            this.button_Disconnect.Location = new System.Drawing.Point(1019, 40);
            this.button_Disconnect.Name = "button_Disconnect";
            this.button_Disconnect.Size = new System.Drawing.Size(99, 30);
            this.button_Disconnect.TabIndex = 8;
            this.button_Disconnect.Text = "Disconnect";
            this.button_Disconnect.UseVisualStyleBackColor = true;
            this.button_Disconnect.Click += new System.EventHandler(this.button_Disconnect_Click);
            // 
            // button_MoveL
            // 
            this.button_MoveL.Enabled = false;
            this.button_MoveL.Location = new System.Drawing.Point(824, 340);
            this.button_MoveL.Name = "button_MoveL";
            this.button_MoveL.Size = new System.Drawing.Size(92, 30);
            this.button_MoveL.TabIndex = 9;
            this.button_MoveL.Text = "Left";
            this.button_MoveL.UseVisualStyleBackColor = true;
            this.button_MoveL.Click += new System.EventHandler(this.button_MoveL_Click);
            // 
            // button_MoveR
            // 
            this.button_MoveR.Enabled = false;
            this.button_MoveR.Location = new System.Drawing.Point(1026, 340);
            this.button_MoveR.Name = "button_MoveR";
            this.button_MoveR.Size = new System.Drawing.Size(92, 30);
            this.button_MoveR.TabIndex = 10;
            this.button_MoveR.Text = "Right";
            this.button_MoveR.UseVisualStyleBackColor = true;
            this.button_MoveR.Click += new System.EventHandler(this.button_MoveR_Click);
            // 
            // button_MoveU
            // 
            this.button_MoveU.Enabled = false;
            this.button_MoveU.Location = new System.Drawing.Point(922, 282);
            this.button_MoveU.Name = "button_MoveU";
            this.button_MoveU.Size = new System.Drawing.Size(92, 30);
            this.button_MoveU.TabIndex = 11;
            this.button_MoveU.Text = "Up";
            this.button_MoveU.UseVisualStyleBackColor = true;
            this.button_MoveU.Click += new System.EventHandler(this.button_MoveU_Click);
            // 
            // button_MoveD
            // 
            this.button_MoveD.Enabled = false;
            this.button_MoveD.Location = new System.Drawing.Point(925, 397);
            this.button_MoveD.Name = "button_MoveD";
            this.button_MoveD.Size = new System.Drawing.Size(92, 30);
            this.button_MoveD.TabIndex = 12;
            this.button_MoveD.Text = "Down";
            this.button_MoveD.UseVisualStyleBackColor = true;
            this.button_MoveD.Click += new System.EventHandler(this.button_MoveD_Click);
            // 
            // button_MoveC
            // 
            this.button_MoveC.Enabled = false;
            this.button_MoveC.Location = new System.Drawing.Point(925, 340);
            this.button_MoveC.Name = "button_MoveC";
            this.button_MoveC.Size = new System.Drawing.Size(92, 30);
            this.button_MoveC.TabIndex = 13;
            this.button_MoveC.Text = "Center";
            this.button_MoveC.UseVisualStyleBackColor = true;
            this.button_MoveC.Click += new System.EventHandler(this.button_MoveC_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(844, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 17);
            this.label4.TabIndex = 15;
            this.label4.Text = "Server";
            // 
            // textBox_ServerPath
            // 
            this.textBox_ServerPath.Location = new System.Drawing.Point(900, 12);
            this.textBox_ServerPath.Name = "textBox_ServerPath";
            this.textBox_ServerPath.Size = new System.Drawing.Size(218, 22);
            this.textBox_ServerPath.TabIndex = 14;
            this.textBox_ServerPath.Text = "tcp://localhost:8080/LogiCam";
            // 
            // button_Login
            // 
            this.button_Login.Enabled = false;
            this.button_Login.Location = new System.Drawing.Point(1019, 113);
            this.button_Login.Name = "button_Login";
            this.button_Login.Size = new System.Drawing.Size(99, 30);
            this.button_Login.TabIndex = 16;
            this.button_Login.Text = "Log in";
            this.button_Login.UseVisualStyleBackColor = true;
            this.button_Login.Click += new System.EventHandler(this.button_Login_Click);
            // 
            // button_Logout
            // 
            this.button_Logout.Enabled = false;
            this.button_Logout.Location = new System.Drawing.Point(1019, 156);
            this.button_Logout.Name = "button_Logout";
            this.button_Logout.Size = new System.Drawing.Size(99, 30);
            this.button_Logout.TabIndex = 17;
            this.button_Logout.Text = "Log out";
            this.button_Logout.UseVisualStyleBackColor = true;
            this.button_Logout.Click += new System.EventHandler(this.button_Logout_Click);
            // 
            // button_MoveLU
            // 
            this.button_MoveLU.Enabled = false;
            this.button_MoveLU.Location = new System.Drawing.Point(824, 282);
            this.button_MoveLU.Name = "button_MoveLU";
            this.button_MoveLU.Size = new System.Drawing.Size(92, 30);
            this.button_MoveLU.TabIndex = 18;
            this.button_MoveLU.Text = "Left Up";
            this.button_MoveLU.UseVisualStyleBackColor = true;
            this.button_MoveLU.Click += new System.EventHandler(this.button_MoveLU_Click);
            // 
            // button_MoveRU
            // 
            this.button_MoveRU.Enabled = false;
            this.button_MoveRU.Location = new System.Drawing.Point(1026, 282);
            this.button_MoveRU.Name = "button_MoveRU";
            this.button_MoveRU.Size = new System.Drawing.Size(92, 30);
            this.button_MoveRU.TabIndex = 19;
            this.button_MoveRU.Text = "Right Up";
            this.button_MoveRU.UseVisualStyleBackColor = true;
            this.button_MoveRU.Click += new System.EventHandler(this.button_MoveRU_Click);
            // 
            // button_MoveLD
            // 
            this.button_MoveLD.Enabled = false;
            this.button_MoveLD.Location = new System.Drawing.Point(824, 397);
            this.button_MoveLD.Name = "button_MoveLD";
            this.button_MoveLD.Size = new System.Drawing.Size(92, 30);
            this.button_MoveLD.TabIndex = 20;
            this.button_MoveLD.Text = "Left Down";
            this.button_MoveLD.UseVisualStyleBackColor = true;
            this.button_MoveLD.Click += new System.EventHandler(this.button_MoveLD_Click);
            // 
            // button_MoveRD
            // 
            this.button_MoveRD.Enabled = false;
            this.button_MoveRD.Location = new System.Drawing.Point(1026, 397);
            this.button_MoveRD.Name = "button_MoveRD";
            this.button_MoveRD.Size = new System.Drawing.Size(92, 30);
            this.button_MoveRD.TabIndex = 21;
            this.button_MoveRD.Text = "Right Down";
            this.button_MoveRD.UseVisualStyleBackColor = true;
            this.button_MoveRD.Click += new System.EventHandler(this.button_MoveRD_Click);
            // 
            // button_Observe
            // 
            this.button_Observe.Enabled = false;
            this.button_Observe.Location = new System.Drawing.Point(925, 462);
            this.button_Observe.Name = "button_Observe";
            this.button_Observe.Size = new System.Drawing.Size(92, 30);
            this.button_Observe.TabIndex = 22;
            this.button_Observe.Text = "Observe";
            this.button_Observe.UseVisualStyleBackColor = true;
            this.button_Observe.Click += new System.EventHandler(this.button_Observe_Click);
            // 
            // button_Snap
            // 
            this.button_Snap.Enabled = false;
            this.button_Snap.Location = new System.Drawing.Point(925, 502);
            this.button_Snap.Name = "button_Snap";
            this.button_Snap.Size = new System.Drawing.Size(92, 30);
            this.button_Snap.TabIndex = 23;
            this.button_Snap.Text = "Snap";
            this.button_Snap.UseVisualStyleBackColor = true;
            this.button_Snap.Click += new System.EventHandler(this.button_Snap_Click);
            // 
            // button_ChangePass
            // 
            this.button_ChangePass.Enabled = false;
            this.button_ChangePass.Location = new System.Drawing.Point(1019, 212);
            this.button_ChangePass.Name = "button_ChangePass";
            this.button_ChangePass.Size = new System.Drawing.Size(99, 30);
            this.button_ChangePass.TabIndex = 26;
            this.button_ChangePass.Text = "Change pass";
            this.button_ChangePass.UseVisualStyleBackColor = true;
            this.button_ChangePass.Click += new System.EventHandler(this.button_ChangePass_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(825, 220);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 17);
            this.label5.TabIndex = 25;
            this.label5.Text = "New Pass";
            // 
            // textBox_NewPassword
            // 
            this.textBox_NewPassword.Location = new System.Drawing.Point(900, 217);
            this.textBox_NewPassword.Name = "textBox_NewPassword";
            this.textBox_NewPassword.PasswordChar = '*';
            this.textBox_NewPassword.Size = new System.Drawing.Size(100, 22);
            this.textBox_NewPassword.TabIndex = 24;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1130, 622);
            this.Controls.Add(this.button_ChangePass);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBox_NewPassword);
            this.Controls.Add(this.button_Snap);
            this.Controls.Add(this.button_Observe);
            this.Controls.Add(this.button_MoveRD);
            this.Controls.Add(this.button_MoveLD);
            this.Controls.Add(this.button_MoveRU);
            this.Controls.Add(this.button_MoveLU);
            this.Controls.Add(this.button_Logout);
            this.Controls.Add(this.button_Login);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox_ServerPath);
            this.Controls.Add(this.button_MoveC);
            this.Controls.Add(this.button_MoveD);
            this.Controls.Add(this.button_MoveU);
            this.Controls.Add(this.button_MoveR);
            this.Controls.Add(this.button_MoveL);
            this.Controls.Add(this.button_Disconnect);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox_UserPassword);
            this.Controls.Add(this.textBox_UserName);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button_Connect);
            this.Name = "Form1";
            this.Text = "Logitech QuickCam Software";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_Connect;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textBox_UserName;
        private System.Windows.Forms.TextBox textBox_UserPassword;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button_Disconnect;
        private System.Windows.Forms.Button button_MoveL;
        private System.Windows.Forms.Button button_MoveR;
        private System.Windows.Forms.Button button_MoveU;
        private System.Windows.Forms.Button button_MoveD;
        private System.Windows.Forms.Button button_MoveC;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_ServerPath;
        private System.Windows.Forms.Button button_Login;
        private System.Windows.Forms.Button button_Logout;
        private System.Windows.Forms.Button button_MoveLU;
        private System.Windows.Forms.Button button_MoveRU;
        private System.Windows.Forms.Button button_MoveLD;
        private System.Windows.Forms.Button button_MoveRD;
        private System.Windows.Forms.Button button_Observe;
        private System.Windows.Forms.Button button_Snap;
        private System.Windows.Forms.Button button_ChangePass;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_NewPassword;
    }
}

