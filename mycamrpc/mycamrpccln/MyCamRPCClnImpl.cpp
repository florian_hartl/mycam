/********************************************************************/
/*																	*/
/* Inhalt:    Implementierung der Unterprogramme des Client 		*/
/*																	*/
/* Autor(en): Hartl Florian (hf)									*/
/* Firma:     Ostbayerische Technische Hochschule Amberg-Weiden		*/
/* Stand:     29.12.2013											*/
/*																	*/
/* Historie:  29.12.2013 hf  erstellt								*/
/*																	*/
/* Copyright 2013-2050 Hartl Florian								*/
/*																	*/
/********************************************************************/



//-----------------------------------------------------------------------------
//Unterfunktionen
//-----------------------------------------------------------------------------

#include <string>
#include <iostream>
using namespace std;

#include "MyCamRPC_i.h"
#include "MyCamRPCClnImpl.h"	

//Umwandlung von string in unsigned char
unsigned char* string_to_uchar(string s_in)
{
	unsigned char* uc_out = (unsigned char*) malloc(s_in.length() + 1);
	size_t i = 0;

	for(i=0; i<s_in.length(); i++)
	{
		uc_out[i] = (unsigned char) s_in.at(i);
	}
	uc_out[i] = '\0';

	return uc_out;
}

//speichert erstes Wort des Inputs in eigenen String und l�scht dieses aus dem Eingabestring
string extract_first_word(string* input)
{
	string firstword = "";
	size_t pos=0;

	pos = input->find(" ");

	firstword = input->substr(0,pos);		//Befehl ist erstes Wort in der Eingabe

	if(pos > input->length())				//letztes Wort in Input erreicht
		input->erase(0, input->length());	//l�schen des letzten Worts; danach nur noch "leerer" string �brig
	else
		input->erase(0,pos+1);				//l�schen des Befehls + Leerzeichen aus dem Eingabestring
	

	return firstword;
}

