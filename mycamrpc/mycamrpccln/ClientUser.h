/********************************************************************/
/*																	*/
/* Inhalt:    Benutzerklasse "ClientUser"							*/
/*																	*/
/* Autor(en): Hartl Florian (hf)									*/
/* Firma:     Ostbayerische Technische Hochschule Amberg-Weiden		*/
/* Stand:     29.12.2013											*/
/*																	*/
/* Historie:  29.12.2013 hf  erstellt								*/
/*																	*/
/* Copyright 2013-2050 Hartl Florian								*/
/*																	*/
/********************************************************************/

#pragma once

#include <iostream>
#include "MyCamRPC_i.h"

#include "MyCamRPCClnImpl.h"
#include "MyCamRPCConfig.h"
#include <string>
#include <sstream>
#include <math.h>
#include <time.h>


using namespace std;

class ClientUser
{
private:

	long ID;
	string name;
	string pw;

public:
	void SetID(long newID);
	long GetID();

	//Methoden mit User-Interaktion
	void com_help();
	void com_user(string name, string pw);
	void com_passwd();
	void com_moveto(string s_x, string s_y);
	void com_movetos(string pos);
	void com_snap(string filename, string interval, string duration);
	void com_observe(string filename, string interations);
	
	//Methoden mit Server-Interaktion
private:				//werden und k�nnen nur intern verwendet werden. Aufruf via com-Methoden
	void passwd();
	void moveto(double x, double y);
	void movetos(string pos);
	void user(string name, string pw);
	RPC_STATUS snap(string filename);
	void snap(string filename, string interval);
	void snap(string filename, string interval, string duration);
	void observe(string filename, string iterations);

	bool checkRPCStatus(RPC_STATUS rpcStatus);

public:
	ClientUser(void);
	~ClientUser(void);
};

