/********************************************************************/
/*																	*/
/* Inhalt:    Hauptprogramm des Client  							*/
/*																	*/
/* Autor(en): Hartl Florian (hf)									*/
/* Firma:     Ostbayerische Technische Hochschule Amberg-Weiden		*/
/* Stand:     29.12.2013											*/
/*																	*/
/* Historie:  13.12.2013 hf  erstellt								*/
/*			  29.12.2013 hf  Unterprogramme ausgelagert             */
/*																	*/
/* Copyright 2013-2050 Hartl Florian								*/
/*																	*/
/********************************************************************/


#include "MyCamRPC_i.h"
#include "MyCamRPCConfig.h"

#include "RpcException.h"

#include "md5.cpp"			//md5 Hash-Funktion

#include "MyCamRPCClnImpl.h"	//Ausgelagerte Unterfunktionen
#include "ClientUser.h"

#include <iostream>
using namespace std;

////-----------------------------------------------------------------------------
////vorw�rtsdeklaration von Unterprogrammen
////-----------------------------------------------------------------------------
//unsigned char* string_to_uchar(string s_in);
//string extract_first_word(string* input);
//
//void changepw(long ID, string actpw);
//void moveto(double x, double y);
//void movetos(string pos);


//-----------------------------------------------------------------------------
//Hauptprogramm
//-----------------------------------------------------------------------------
int main(int argc, char *argv[])
{
//-----------------------------------------------------------------------------
//Variablendeklaration
//-----------------------------------------------------------------------------

	ClientUser actUser;		//aktueller Benutzer des Clients

	string userinput;
	string command;
	string para1;
	string para2;
	string para3;



	unsigned char* buf = NULL;
	
	RPC_STATUS rpcStatus = 0;							//R�ckgabewert der RPC Funktionen -> Fehlerbehandlung
	unsigned char* stringBinding = NULL;				//String zum Erstellen des Binding Handles
	
	char* srv_netwaddr;									//Server IP-Addresse			
	char* str_RPC_ENDPOINT = (char*) RPC_ENDPOINT;		//Server Port nur zur Ausgabe

	//MD5_CTX myMD5;
	//unsigned char buf[16];

	//MD5_Init(&myMD5);
	//MD5_Update(&myMD5,"Hallo",6);
	//MD5_Final(buf,&myMD5);

	//cout << buf << endl;


//-----------------------------------------------------------------------------
//Einlesen der Server IP-Addresse aus Anwendungs-Parameter
//-----------------------------------------------------------------------------

	if (argc>1) 
	{
		srv_netwaddr=argv[1];		//IP-Addresse ist erstes Argument der .exe
	}
	else 
	{
		srv_netwaddr = RPC_SRV_NETWADDR;	//wird keine IP-Addresse angegeben, wird RPC_SRV_NETWADDR default gesetzt (localhost)
	}

	string str_RPC_SRV_NETWADDR(srv_netwaddr);		//Umwandeln der IP-Addresse in string zur Ausgabe

//-----------------------------------------------------------------------------
//Verbindungsaufbau
//-----------------------------------------------------------------------------

	try
	{
		//Erzeugen der Stringdarstellung des Binding-Handles
		rpcStatus = RpcStringBindingCompose(NULL,
											(UCHAR*) RPC_PROT_SEQ,
											(UCHAR*) srv_netwaddr,
											(UCHAR*) RPC_ENDPOINT,
											NULL,
											&stringBinding);
		if(rpcStatus == RPC_S_OK)
			cout << "Erzeugen der Stringdarstellung des Binding-Handles erfolgreich" << endl;
		else
			throw(RpcException(rpcStatus,(string)"RpcStringBindingCompose fuer Server " +str_RPC_SRV_NETWADDR+":"+str_RPC_ENDPOINT+"\n(Erzeugen der Stringdarstellung des Binding-Handles) failed", "RPC Runtime Error"));


		//Erzeugen des Binding-Handles
		rpcStatus = RpcBindingFromStringBinding(stringBinding,&hMyCamRPC_i);
		if(rpcStatus == RPC_S_OK)
			cout << "Erzeugen des Binding-Handles erfolgreich" << endl;
		else
			throw(RpcException(rpcStatus,(string)"RpcBindingFromStringBinding fuer Server " +str_RPC_SRV_NETWADDR+":"+str_RPC_ENDPOINT+"\n(Erzeugen des Binding-Handles) failed", "RPC Runtime Error"));

		//Freigabe der Stringdarstellung
		rpcStatus = RpcStringFree(&stringBinding);
		if(rpcStatus == RPC_S_OK)
			cout << "Freigabe der Stringdarstellung des Binding-Handles erfolgreich" << endl;
		else
			throw(RpcException(rpcStatus,(string)"RpcStringFree fuer Server " +str_RPC_SRV_NETWADDR+":"+str_RPC_ENDPOINT+"\n(Freigabe der Stringdarstellung des Binding-Handles) failed", "RPC Runtime Error"));
										
//-----------------------------------------------------------------------------
//Aufrufen der RPC-Funktionen
//-----------------------------------------------------------------------------
		cout << "Bereit fuer Eingabe" << endl;

		try
		{

			while(true)	//Endlolsschleife wird durch Usereingabe abgebrochen
			{
				getline(cin, userinput);					//Einlesen des Userbefehls mit seinen Parametern

				//Extrahieren des Befehls und dessen Parameter aus der Eingabe
				command = extract_first_word(&userinput);
				para1 = extract_first_word(&userinput);
				para2 = extract_first_word(&userinput);
				para3 = extract_first_word(&userinput);

				if(command == "exit")
					break;
				else if(command == "help")
				{
					actUser.com_help();
				}
				else if(command == "user")
				{
					actUser.com_user(para1, para2);
				}
				else if(command == "passwd")
				{
					
					actUser.com_passwd();
				}
				else if(command == "moveto")
				{
					actUser.com_moveto(para1, para2);
				}
				else if(command == "movetos")
				{
					actUser.com_movetos(para1);
				}
				else if(command == "snap")
				{
					actUser.com_snap(para1, para2, para3);
				}
				else if(command == "observe")
				{
					actUser.com_observe(para1, para2);
				}
				else
					cout << command << " unbekannter Befehl (fuer Hilfe help eingeben)" << endl;
			}

		}
		catch(...)
		{
			//TODO: Warum funktioniert das nicht ??? (wenn z.b. server nicht gestartet war) -> statt Try catch RPCTRY etc verwenden
			cout << "Fehler bei Funktionsaufruf" << endl;
			throw(RpcException(rpcStatus,(string)"Funktionsaufruf gescheitert", "RPC Runtime Error"));
		}
		


		system("pause");

		//Beenden des Servers	TODO: aus Client entfernen!
		ShutdownSrv();
	}
//-----------------------------------------------------------------------------
//Fehlerbehandlung bei RPC-Exceptions
//-----------------------------------------------------------------------------

	catch(RpcException &e)
	{
		cout << e.GetErrorText() << endl;
		cout << "Fehler Nr: " << e.GetStatus() << endl;
	}


	system("pause");
	return 0;
}


