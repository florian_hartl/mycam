/********************************************************************/
/*																	*/
/* Inhalt:    Implementierung Benutzerklasse "ClientUser"			*/
/*																	*/
/* Autor(en): Hartl Florian (hf)									*/
/* Firma:     Ostbayerische Technische Hochschule Amberg-Weiden		*/
/* Stand:     29.12.2013											*/
/*																	*/
/* Historie:  29.12.2013 hf  erstellt								*/
/*			  09.01.2014 hf  Funkionsvarianten f�r snap und observe */ 														
/*																	*/
/* Copyright 2013-2050 Hartl Florian								*/
/*																	*/
/********************************************************************/

#include "ClientUser.h"


double str_to_double(string input);
int str_to_int(string input);
int str_to_singleint(string input);
string int_to_str(int input);


//-----------------------------------------------------------------------------
//Methoden zur Umsetzung des eingegebenen Befehls (command = com)
//-----------------------------------------------------------------------------


void ClientUser::com_help()
{
	cout << "Hilfe zur Bedienung der Remote-Kamera-Steuerung" << endl;
	cout << "Befehl und optional Parameter getrennt durch Leerzeichen eingeben" << endl;
	cout << "Detaillierte Hilfe (wenn vorhanden *) durch Eingabe von \"Befehl help\"" << endl << endl;

	cout << "Befehl \t \t Beschreibung" << endl;
	cout << "-------------------------------------------------------------" << endl;
	cout << "user \t \t Einloggen am Server *" << endl;
	cout << "passwd \t \t Aendern des Userpassworts" << endl;
	cout << "moveto \t \t Bewegen der Kamera durch Koordinatenangabe *" << endl;
	cout << "movetos \t Bewegen der Kamera durch Positionsangabe *" << endl;
	cout << "snap \t \t Aufnehmen von Bildern *" << endl;
	cout << "observe \t automatische Ueberwachung *" << endl;
	cout << "exit \t \t Beenden des Clients" << endl;
}

void ClientUser::com_user(string name, string pw)
{
	if(name == "help" && pw == "")
	{
		cout << "Einloggen am Server durch \"user Username Userpasswort\"" << endl;
	}
	else
	{
		cout << "Versuche einloggen am Server mit Username: " << name << " und Passwort: " << pw << endl;	//TODO: PW verstecken
		this->user(name, pw);
	}
	return;
}


void ClientUser::com_passwd()
{
	cout << "Aendern des Passworts" << endl;
	passwd();
}

void ClientUser::com_moveto(string s_x, string s_y)
{
	double x=0;
	double y=0;

	if(s_x == "help" || s_x == "" || s_y =="")
	{
		cout << "Kameraposition mit zwei Parametern von -1 bis 1 eingeben:"<<endl;
		cout << "Parameter 1: gewuenschte Kameraposition rechts (-1) bis links (1)" << endl;
		cout << "Parameter 2: gewuenschte Kameraposition unten (-1) bis oben (1)" << endl;
	}
	else
	{
		cout << "Bewegen der Kamera auf X-Pos: " << s_x << " und Y-Pos: " << s_y << endl;

		x = str_to_double(s_x);
		y = str_to_double(s_y);

		moveto(x, y);
	}

	return;
}

void ClientUser::com_movetos(string pos)
{
	if(pos == "help")
	{
		cout << "Kameraposition durch Positionsnamen festlegen" << endl;
		cout << "Moegliche Namen:" << endl;
		cout << "lt \t = left top" << endl;
		cout << "lb \t = left bottom" << endl;
		cout << "center \t = center" << endl;
		cout << "rt \t = right top" << endl;
		cout << "rb \t = right bottom" << endl;
	}
	else
	{
		cout << "Bewegen der Kamera auf Pos: " << pos << endl;
		movetos(pos);
	}
	return;
}

void ClientUser::com_snap(string filename, string interval, string duration)
{
	if(filename == "help" && interval == "" && duration == "")
	{
		cout << "Aufnahme und Abspeichern von Bildern" << endl;
		cout << "Parameter 1: Dateiname" << endl;
		cout << "Parameter 2 (optional): Intervallangabe in Sekunden bei Serienaufnahme" << endl;
		cout << "Parameter 3 (optional): Dauer der Serienaufnahme in Sekunden" << endl;
	}
	else
	{
		if(filename.find_first_not_of("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_") > filename.length())
		{

			cout << "Aufnahme von Bildern" << endl;
			if(filename == "")
			{
				cout << "Bitte Dateiname noch angeben:" << endl;
				getline(cin, filename);
			}

			if(interval == "")
			{
				cout << "Einzelaufnahme eines Bildes" << endl;
				this->snap(filename);
			}
			else if(duration == "")
			{
				cout << "Endlose Serienaufnahme von Bildern im Intervall von "<< interval << " Sekunden" << endl;
				this->snap(filename, interval);
			}
			else
			{
				cout << "Serienaufnahme von Bildern im Intervall von "<< interval << " Sekunden und " << duration << " Sekunden lang" << endl;
				this->snap(filename, interval, duration);
			}
		}
		else
		{
			cout << "Kein gueltiger Dateiname. Bitte keine Sonderzeichen verwenden" << endl;
			return;
		}

	}
	return;
}

void ClientUser::com_observe(string filename, string iterations)
{
	int iIterations = 1;	//Default ist eine Wiederholung

	if(filename == "")
	{
		cout << "Dateiname bitte noch angeben:" << endl;
		getline(cin, filename);
	}

	if(filename.find_first_not_of("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_") > filename.length())
	{

		if(filename == "help" && iterations =="")
		{
			cout << "Autom. Bewegungsfahrt der Kamera, dabei Aufnahme und Abspeichern von Bildern" << endl;
			cout << "Parameter 1: Dateiname" << endl;
			cout << "Parameter 2 (optional): Anzahl der Wiederholungen der Bewegungsfahrt" << endl;

		}
		else
		{

			cout << "Automatische Beobachtung" << endl;
			this->observe(filename, iterations);
		}
	}
	else
	{
			cout << "Kein gueltiger Dateiname. Bitte keine Sonderzeichen verwenden" << endl;
			return;
	}
	return;
}

//-----------------------------------------------------------------------------
//Unterfunktionen mit Schnittstelle zum Server
//-----------------------------------------------------------------------------

//�ndern des User-Passworts, Parameter: Sitzungs-ID, aktuelles Passwort
void ClientUser::passwd()			
{
	string input;								//Eingabestring
	string control_input;						//Eingabestring zur Kontrolle der Eingabe
	RPC_STATUS rpcStatus;						//Kontrolle ob RPC-Aufruf erfolgreich war

	cout << "Passwort aendern" << endl;

	cout << "altes PW eingeben! " << endl;
	cin >> input;								//Einlesen des alten Passworts

	if(input == this->pw)							//Vergleich mit aktuellem Passwort des Users
	{
		cout << "korrekt" << endl;
		cout << "neues PW eingeben" << endl;
		cin >> input;													//Einlesen des neuen Passworts
		cout << "erneute eingabe" << endl;
		cin >> control_input;											//Erneute Eingabe des neuen Passworts
		if(input == control_input)										//Kontrolle ob beide Eingaben �bereinstimmen
		{
			cout << "Passwort wird geaendert" << endl;
			rpcStatus = changepw(this->ID, string_to_uchar(input));			//RPC-Funktion zum setzen des PW auf dem Server TODO: Fehlerbehandlung
			if(rpcStatus == RPC_S_OK)
			{
				cout << "Passwort wurde erfolgreich geandert" << endl;
				this->pw = input;												//internes setzen des Passworts
			}
			else
			{
				cout << "Fehler bei RPC-Aufruf changepw: " << endl;
				this->checkRPCStatus(rpcStatus);
			}

		}
		else
		{
			cout << "Eingaben stimmen nicht ueberein" << endl;
		}

	}
	else
	{
		cout << "Falsches Passwort" << endl;
	}
	return;
}

//Bewegung der Kamera durch Koordinatenangabe
void ClientUser::moveto(double x, double y)
{
	RPC_STATUS rpcStatus;

	if(x > 1.0 || x < -1.0 || y > 1.0 || y < -1.0)	//Pr�fen ob gew�nschte Bewegung innerhalb des Wertebereichs -1 .. 1 liegt
	{
		cout << "Bewegung bitte im Bereich -1 bis 1 eingeben" << endl;
		return;
	}

	//TODO: Kontrolle x,y g�ltiger wertebereich; R�ckgabe vom Server? ...
	rpcStatus = move(this->ID, x, y);
	this->checkRPCStatus(rpcStatus);
	return;
}

//Bewegung der Kamera durch Positionsbezeichnungen
void ClientUser::movetos(string pos)
{
	double x=0;
	double y=0;
	RPC_STATUS rpcStatus;

	//Auswerten der Position 
	if(pos == "lb")
	{
		x = -1;
		y = -1;
	}
	else if(pos == "lt")
	{
		x = -1;
		y = 1;
	}
	else if(pos == "center")
	{
		x = 0;
		y = 0;
	}
	else if(pos == "rb")
	{
		x = 1;
		y = -1;
	}
	else if(pos == "rt")
	{
		x = 1;
		y = 1;
	}
	else
	{
		cout << "keine gueltige Positionsbezeichnung" << endl;
		return;
	}
	
	rpcStatus = move(this->ID, x, y);			//Aufruf der eigentlichen RPC-Funktion
	this->checkRPCStatus(rpcStatus);

	return;
}


//login am Server
void ClientUser::user(string name, string pw)
{
	this->ID = login(string_to_uchar(name),string_to_uchar(pw));	//Aufruf der RPC-Funktion

	//Auswerten der R�ckgabe (im Fehlerfall wird Fehler Codiert mit negativen Werten �bertragen)
	if(ID > 0)	//G�ltige ID
	{
		cout << "Login erfolgreich!" << endl;

		//Aktualisieren des Usernamens und Passworts auf Client-Seite
		this->name = name;		
		this->pw = pw;
	}
	else if(ID == LOGIN_ERR_NOUSER)
	{
		cout << "Fehler bei RPC-Aufruf login: Kein User mit diesem Namen gefunden" << endl;
	}
	else if(ID == LOGIN_ERR_WRONGPW)
	{
		cout << "Fehler bei RPC-Aufruf login: Falsches Passwort" << endl;
	}
	else
	{
		cout << "Fehler bei RPC-Aufruf login" << endl;
	}

	return;
}

RPC_STATUS ClientUser::snap(string filename)
{

	cout << "CHEESE" << endl;
	unsigned char header[54];
	unsigned char data[480*640*3];
	RPC_STATUS rpcstatus;
	FILE *Image;


	memset(header,0,54);
	memset(data,0,480*640*3);

	rpcstatus = takeimage(this->ID, 54, 480*640*3, header, data);
	if(this->checkRPCStatus(rpcstatus))
	{
		return rpcstatus;
	}
	

	filename += ".bmp";
	Image = fopen(filename.data(),"wb");

	fwrite(header, 1, 54, Image);	
	fwrite(data,1,480*640*3, Image);
	fclose(Image);


	////TEST: TODO wieder l�schen
	//Image=fopen(filename.data(),"rb");
	//fseek(Image,0,SEEK_SET);			//Setzen des "Cursors" auf den Anfang des eingelesenen (Datei-)Streams          
 //   num = fread(&bMapFileHeader,sizeof(BITMAPFILEHEADER),1,Image);	//Lesen des File Headers
 //   num = fread(&bMapInfoHeader,sizeof(BITMAPINFOHEADER),1,Image);	//Lesen des Info Headers
	//fclose(Image);

	return rpcstatus;
}

void ClientUser::snap(string filename, string interval)
{
	int i_interval;
	int i=0;
	string str_temp;
	RPC_STATUS rpcStatus = 0;
	clock_t start;
	clock_t stop;
	clock_t delay;
	DWORD pause;


	i_interval = str_to_int(interval);
	

	for(i=1;;i++)
	{
		str_temp = int_to_str(i);			//konvertieren der Z�hlervariablen (int) in einen String
		str_temp = filename + str_temp;		//Dateinamen Nummer anf�gen

		start = clock();						//Zeitmessung starten
		rpcStatus = this->snap(str_temp);				//Einzelnes Bild aufnehmen und abspeichern
		if(this->checkRPCStatus(rpcStatus))
			break;

		stop = clock();						//Zeitmessung stoppen
		delay = (stop - start)*1000/CLOCKS_PER_SEC;
		//Pausenzeit berechnen, wodurch gew�nschtes Intervall erreicht wird:
		pause = 1000 * i_interval;			
		if(delay > pause)
			pause = 0;
		else
			pause -= delay;
		Sleep(pause);

		if(i > 10)
			break;		//Zu Testzwecken werden nur 11 Bilder gemacht, statt unendlich vielen TODO: l�schen

	}

	return;
}

void ClientUser::snap(string filename, string interval, string duration)
{
	int i_interval;
	int i_duration;
	int i=0;
	string str_temp;
	RPC_STATUS rpcStatus;

	clock_t start;
	clock_t stop;
	clock_t delay;
	DWORD pause;

	i_interval = str_to_int(interval);
	i_duration = str_to_int(duration);

	i_duration = i_duration/i_interval;	//Anzahl der Bilder berechnen.

	for(i=1; i <= i_duration;i++)
	{
		str_temp = int_to_str(i);			//konvertieren der Z�hlervariablen (int) in einen String
		str_temp = filename + str_temp;		//Dateinamen Nummer anf�gen

		start = clock();						//Zeitmessung starten
		rpcStatus = this->snap(str_temp);				//Einzelnes Bild aufnehmen und abspeichern
		if(this->checkRPCStatus(rpcStatus))
			break;

		stop = clock();						//Zeitmessung stoppen
		//Pausenzeit berechnen, wodurch gew�nschtes Intervall erreicht wird:
		pause = 1000 * i_interval;	
		delay = (stop - start)*1000/CLOCKS_PER_SEC;
		if(delay > pause)
			pause = 0;
		else
			pause -= delay;
		Sleep(pause);
	}



	return;
}


void ClientUser::observe(string filename, string iterations)
{
	int iIterations = 1;
	string str_temp_filename;
	time_t now;

	if(iterations != "")	//Wenn Wiederholungen angegeben wurden
	{
		iIterations = str_to_int(iterations);
	}

	while(iIterations > 0)
	{
		cout << "Beobachtungsfahrt gestartet. Noch " << iIterations << "Fahrt(en)." << endl;
		this->movetos("center");
		//TOOD: warten bis bewegung fertig. ggf durch R�ckmeldung vom Server
		time(&now);
		str_temp_filename = filename + int_to_str((int)now);
		this->snap(str_temp_filename);
		
		this->movetos("lb");
		time(&now);
		str_temp_filename = filename + int_to_str((int)now);
		this->snap(str_temp_filename);

		this->movetos("lt");
		time(&now);
		str_temp_filename = filename + int_to_str((int)now);
		this->snap(str_temp_filename);

		this->movetos("rt");
		time(&now);
		str_temp_filename = filename + int_to_str((int)now);
		this->snap(str_temp_filename);

		this->movetos("rb");
		time(&now);
		str_temp_filename = filename + int_to_str((int)now);
		this->snap(str_temp_filename);

		iIterations--;
	}
}

//Konstruktor
ClientUser::ClientUser(void)
{
	this->ID = 0;
	this->name = "";
	this->pw = "";
}

//Dekonstruktor
ClientUser::~ClientUser(void)
{
}

//-----------------------------------------------------------------------------
//Hilfsfunktionen
//-----------------------------------------------------------------------------

//Umwandeln eines Strings mit .-Darstellung in einen double Wert
double str_to_double(string input)
{
	double output = 0;
	double sign = 1;		//Vorzeichen
	size_t pos;
	size_t i;
	string sTemp;

	if(input.at(0) == '-')	//negative Zahl
	{
		sign = -1;
		input = input.erase(0,1);
	}

	if(input.find_first_not_of("0123456789.") < input.length())
	{
		cout << "Bitte nur Ziffern und ggf. einen . verwenden!" << endl;
		return 0;		//TODO: besser w�re exception?
	}

	pos = input.find(".");

	//Pr�fe, ob zwei . eingegeben wurden
	sTemp = input.substr(pos + 1, input.length());	//restlichen String ohne ersten . zwischenspeichern
	if(sTemp.find_first_not_of("0123456789") < input.length())		
	{
		cout << "Bitte nur Ziffern und EINEN . verwenden!" << endl;
		return 0;		//TODO: besser w�re exception?
	}

	if(pos > input.length())
	{
		pos = input.length();

	}

	for(i = 0; i < pos; i++)
	{
		sTemp = input.at(i);

		output += str_to_singleint(sTemp) * pow(10, (pos - i - 1));
	}

	for(i = 1; i + pos < input.length(); i++)
	{
		sTemp = input.at(i + pos);
		output += str_to_singleint(sTemp) / pow(10, i);
	}

	output *= sign;		//mit Vorzeichen multiplizieren

	cout << output << endl;
	return output;
}

//Umwandeln eines Strings in einen int-Wert
int str_to_int(string input)
{
	int output = 0;
	int sign = 1;		//Vorzeichen
	size_t i;
	string sTemp;

	if(input.at(0) == '-')	//negative Zahl
	{
		sign = -1;
		input = input.erase(0,1);
	}

	//Auf g�ltige Zeichen pr�fen
	if(input.find_first_not_of("0123456789") < input.length())
	{
		cout << "Bitte nur Ziffern verwenden!" << endl;
		return 0;		//TODO: besser w�re exception?
	}

	for(i = 0; i < input.length(); i++)
	{
		sTemp = input.at(i);

		output += str_to_singleint(sTemp) * (int) pow(10, (input.length() - i - 1));
	}

	output *= sign;

	return output;
}

//Umwandeln eines einzelnen Zeichen in eine Integerzahl
int str_to_singleint(string input)
{
	int output = 0;
	int iTemp = 0;

	iTemp = (int) input.at(0);

	if(iTemp >= 48 && iTemp <= 57)	//ist Input eine Zahl?
	{
		output = iTemp - 48;
	}

	return output;
}

//konvertieren von int in einen String
string int_to_str(int input)
{
	string output;

    string str_zaehler;
    stringstream integer_string_stringstream;			//string stream zum zwischenspeichern
 
    integer_string_stringstream << input;
    integer_string_stringstream >> str_zaehler;
  //  cout << str_zaehler << endl;

	output = str_zaehler;

	return output;
}

bool ClientUser::checkRPCStatus(RPC_STATUS rpcStatus)
{
	if(rpcStatus != RPC_S_OK)
	{
		cout << "Fehler bei RPC-Aufruf" << endl;			//TODO: Fehlernummer in Text �bersetzen
		if(rpcStatus == RPC_S_TIMEOUT)
		{
			cout << "Timeout. Bitte nochmals am Server anmelden (user...)" << endl;
		}
		else if(rpcStatus == ERR_BADID)
		{
			cout << "Keine gueltige Sitzungs-ID. Bitte nochmals am Server anmelden (user...)" << endl;
		}
		else if (rpcStatus == CAM_ERR_IN_USE)
		{
			cout << "Kamera bereits in Gebrauch" << endl;
		}
		//TODO: weitere Fehler?
		else if (rpcStatus == CAM_ERR)
		{
			cout << "sonstiger Fehler bei Kamera" << endl;
		}
		else
		{
			cout << "Unbekannter Fehler" << endl;
		}
		return 1;
	}
	return 0;
}