/********************************************************************/
/*																	*/
/* Inhalt:    Unterprogramme des Client 							*/
/*																	*/
/* Autor(en): Hartl Florian (hf)									*/
/* Firma:     Ostbayerische Technische Hochschule Amberg-Weiden		*/
/* Stand:     29.12.2013											*/
/*																	*/
/* Historie:  29.12.2013 hf  erstellt								*/
/*																	*/
/* Copyright 2013-2050 Hartl Florian								*/
/*																	*/
/********************************************************************/

#pragma once

#include <iostream>
using namespace std;

//TODO zu Klasse umbauen? eigene Klasse mit folgenden Methoden, als attribute möglicherweise ID ???

	unsigned char* string_to_uchar(string s_in);
	string extract_first_word(string* input);