/********************************************************************/
/*																	*/
/* Inhalt:    Implementierung der Benutzerklasse "Usermanager"		*/
/*																	*/
/* Autor(en): Hartl Florian (hf)									*/
/* Firma:     Ostbayerische Technische Hochschule Amberg-Weiden		*/
/* Stand:     18.12.2013											*/
/*																	*/
/* Historie:  17.12.2013 hf		erstellt							*/
/*            18.12.2013 hf		Verbessern der load/save Methode	*/
/*								Dekonstruktor erweitert				*/
/*			  19.12.2013 hf		umstellung von char* auf string		*/
/*			  28.12.2013 hf		erweiterung der Methoden     		*/
/*																	*/
/* Copyright 2013-2050 Hartl Florian								*/
/*																	*/
/********************************************************************/


#include "Usermanager.h"
#include <fstream>
#include <iostream>
#include <random>
#include <time.h>

using namespace std;

	

//-----------------------------------------------------------------------------
//Laden der User-Liste aus der .txt-Datei
//-----------------------------------------------------------------------------
void Usermanager::load(void)
{
	fstream UserFile;

	std::string zeile;
	std::string name;
	std::string pw;
	
	User* pUserTemp=NULL;

	UserFile.open("UserFile.txt", ios::in);
	if(UserFile.good())		//�ffnen erfolgreich
	{
		UserFile.seekg(0L, ios::beg);		//An den Anfang der Datei springen

		

		//Datei Zeilenweise auslesen (in jeder Zeile steht User Name und PW)
		while(true)
		{	
			getline(UserFile,zeile);		//ganze Zeile einlesen

			if(zeile == "EOF")
				break;						//ist die eingelesene Zeile der Ende-Marker, wird das Einlesen der Datei abgebrochen

			name = zeile.substr(0,zeile.find(";"));		//Name herausfiltern (erster Zeilen-Teil bis ;)		

			pw = zeile.substr(zeile.find(";") + 1);
			
			if(this->FirstUser == NULL)		//wurde noch kein User angelegt, wird der erste User erzeugt:
			{
				this->FirstUser = new User(name,pw);	//neuen User erzeugen mit Name und Passwort
				pUserTemp = this->FirstUser;			//Zwischenspeichern des zuletzt erzeugten Users
			}
			else
			{
				pUserTemp->SetNextUser(new User(name,pw));	//neuen User erzeugen mit Name und Passwort
				pUserTemp = pUserTemp->GetNextUser();		//Zwischenspeichern des zuletzt erzeugten Users
			}



		}//ENDE schleife �ber alle Zeilen der User-Datei

	}
	else
		cout << "Fehler beim Oeffnen der UserFile.txt" << endl;

	UserFile.close();
}



//-----------------------------------------------------------------------------
//Speichern der User-Liste in die .txt-Datei
//-----------------------------------------------------------------------------
void Usermanager::save(void)
{
	User* pUserTail = NULL;		//Zeiger zum Durchlaufen der verketteten User-Liste
	std::string buf;			//Puffer-string

	fstream UserFile;			//Filestream der txt Datei

	//TODO: �berpr�fen ob datei schon ge�ffnet -> critical section? zweite control-datei?
	UserFile.open("UserFile.txt", ios::out);		//�ffnen der txt Datei

	if(UserFile.good())		//�ffnen erfolgreich?
	{
		
		UserFile.seekg(0L, ios::beg);		//An den Anfang der Datei springen

		for(pUserTail = this->FirstUser; pUserTail != NULL; pUserTail = pUserTail->GetNextUser())		//Durchlaufen der Verketteten User-Liste
		{
			//Zusammenstellen der Zeile im buf-string: User Name+;+Passwort+neue Zeile 
			buf = pUserTail->GetName() + ";" + pUserTail->GetPassword() + "\n";	

			UserFile << buf;	//Schreiben der Zeile in txt Datei
		}

		UserFile<<"EOF";

	}//ENDE Abfrage �ffnen erfolgreich
	else
		cout << "Fehler beim Oeffnen der UserFile.txt" << endl;
	
	UserFile.close();	//Schlie�en der txt Datei
}

//-----------------------------------------------------------------------------
//Login Funktion
//-----------------------------------------------------------------------------
long Usermanager::login(std::string name, std::string pw)		//TODO: R�ckgabewert notwendig???
{
	User* pUserTail = NULL;
	long ID=0;					//R�ckgabewert beinhaltet ID bzw. Fehlermeldung

	time_t actTime = 0;			//aktuelle Systemzeit

	for(pUserTail = this->FirstUser; pUserTail != NULL; pUserTail = pUserTail->GetNextUser())	//Durchlaufen der User-Liste
	{
		if(name == pUserTail->GetName())	//stimmen eingegebener Name und Name eines Users �berein?
		{
			if(pw == pUserTail->GetPassword())	//stimmt dazu auch noch das Passwort
			{

				//TODO: was pasiert wenn zweimal der selber user sich einloggt???

				cout << "Userpasswort richtig!" << endl;
				
				pUserTail->SetLastaction();						//Aktalisieren der letzten Aktionszeit
				return ID = pUserTail->randomID();				//W�rfeln der ID
								
			}
			else
			{
				cout << "Passwort falsch!" << endl;
				ID = LOGIN_ERR_WRONGPW;
				return ID;
			}
		}//ENDE Abfrage User-name gefunden
		
	}//ENDE Durchlaufen der User-Liste

	cout << "Kein User mit diesem Namen gefunden!" << endl;
	ID = LOGIN_ERR_NOUSER;
	return ID;

}

//Pr�fen der �bergebenen ID auf G�ltigkeit
User* Usermanager::checkID(long ID)
{
	User* pUserTail;

	for(pUserTail = this->GetFirstUser(); pUserTail!= NULL; pUserTail = pUserTail->GetNextUser())	//Durchlaufen aller User
	{
		if(pUserTail->GetID() == ID)
		{
			return pUserTail;
		}
	}//ENDE durchlaufen aller User


	return NULL;		//Falls kein User mit dieser ID gefunden wurde
}