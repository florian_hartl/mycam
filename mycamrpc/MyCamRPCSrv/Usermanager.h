/********************************************************************/
/*																	*/
/* Inhalt:   Benutzerklasse "Usermanager"							*/
/*																	*/
/* Autor(en): Hartl Florian (hf)									*/
/* Firma:     Ostbayerische Technische Hochschule Amberg-Weiden		*/
/* Stand:     18.12.2013											*/
/*																	*/
/* Historie:  17.12.2013 hf		erstellt							*/
/*			  19.12.2013 hf		umstellung von char* auf string		*/	
/*																	*/
/* Copyright 2013-2050 Hartl Florian								*/
/*																	*/
/********************************************************************/

#pragma once



#include <string>
#include "User.h"
#include "Cam.h"
#include "MyCamRPCConfig.h"		//Definition der Fehlermeldungen (z.B. f�r ID return)

#include <Windows.h>

#include <iostream>
using namespace std;


//static zur Umsetzung des Singleton-Patterns. Es existiert nur 1 Usermanager!
class Usermanager
{
private:
	static Usermanager* singleton_instance;
	static int instanceCounter;
	static CRITICAL_SECTION CS;

	User* FirstUser;	
	Cam* pCam;
	

	Usermanager(void)
	{ 	
		this->FirstUser = NULL;
		this->instanceCounter = 0;
		this->load();	//laden der User-Liste von HDD
		this->pCam = new Cam;
	}
	~Usermanager(void)
	{  
		cout << "Dekonstruktor Usermanager" << endl;	//TODO: wieder l�schen
		this->save();	//speichern der User-Liste auf HDD
	
		if(this->FirstUser != NULL)
			delete this->FirstUser;		//L�schen der Gesamten verketteten Liste, da der erste User auch den n�chsten User l�scht (falls vorhanden)
	}

public:
	static Usermanager* GetInstance()	//"Erzeugen" einer Singleton Instanz
	{
		if(singleton_instance == NULL)
			InitializeCriticalSection(&CS);

		EnterCriticalSection(&CS);
		if(singleton_instance == NULL)
			singleton_instance = new Usermanager();
		
		LeaveCriticalSection(&CS);

		instanceCounter++;

		return singleton_instance;
	}

	static void release()				//Freigeben der Singleton Klasse, wenn nicht mehr ben�tigt
	{
		instanceCounter--;
		if(instanceCounter == 0)
			delete singleton_instance;

	}

	User* GetFirstUser(){return this->FirstUser;};
	Cam* GetCam(){return this->pCam;};
	long login(std::string name, std::string pw);

	void load();
	void save();
	User* checkID(long ID);
	
	

	
};



//Singleton Template aus �Design Patterns. Elements of Reusable Objectoriented Software.�
//class Singleton
//{
//   public:
//     static Singleton* exemplar();
//
//   protected:
//     Singleton() {}
//
//   private:
//     static Singleton *instanz;
//};
//
//Singleton* Singleton::instanz = 0;
//
//Singleton* Singleton::exemplar()
//{
//  if( instanz == 0 )
//    instanz = new Singleton();
//  return instanz;
//}