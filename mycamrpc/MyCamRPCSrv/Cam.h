/************************************************************/
/*                                                          */
/* Inhalt: Klasse Cam f�r									*/
/*	       Ansteuerung f�r Logimove_Dll                     */
/*                                                          */
/*                                                          */
/*                                                          */
/*                                                          */
/*                                                          */
/* Autor(en)    : Ries Stefan                               */
/*                Sailer Andreas                            */
/* In Klasse transferiert von Hartl Florian					*/
/*															*/
/* Firma        : Hochschule Amberg-Weiden                  */
/* Abteilung    : EI / Software-Projektelabor               */
/*                                                          */
/* Stand        : 07. April 2009                            */
/*                                                          */
/* Historie     : 16.05.2006	erstellt                    */
/*                07.04.2009	erweitert um die Unter-     */
/*                              stuetzung der Logitech      */
/*                              Sphere AF                   */
/*				  02.01.2014 hf	In Klasse eingebettet		*/
/*                                                          */
/* Copyright 2006 Josef P�sl, HAW Amberg-Weiden             */
/*                                                          */
/************************************************************/

#pragma once

#include "Logimove_Dll.h"
#include "Windows.h"
#include <iostream>
using namespace std;

#include <conio.h>
#include <malloc.h>
#include <windows.h>

#include "ImageCaptureDll.h"
#include <atlbase.h>
#include <comutil.h>
#include <fstream>

#include <Windows.h>

#include "MyCamRPCConfig.h"



class Cam
{
private:

	//f�r Kamerabewegung
	int input_error;
	int devnumber;
	int connected_Devices;
	char movement;

	HRESULT Result;
	HANDLE Device;
	HANDLE* ptr_Device;
	char* devString;
	int BufferSize;

	double actYPos;
	double actXPos;

	//f�r Bildaufnahme
	HRESULT hr;
	CComPtr<IEnumGrabDevices> grabDevices;
	CComPtr<IGrabDevice> grabDevice;
	CComPtr<IImage> img;

public:
	Cam(void);
	~Cam(void);

	unsigned char header[54];				//Header der Bilddatei als string (nicht als komplexe struktur)
	unsigned char data[480*640*3];			//"3D" Array zum Speichern der Bilddateien (480*640 Pixel in 3 Farbkan�len)

	void image();	
	void move(double xend, double yend);
	void stepleft();
	void stepright();
	void stepup();
	void stepdown();

	HRESULT GetResult(){return this->Result;};
};

