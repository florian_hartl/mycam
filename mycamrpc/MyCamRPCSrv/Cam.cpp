/************************************************************/
/*                                                          */
/* Inhalt: Implementierung der Klasse Cam f�r				*/
/*	       Ansteuerung f�r Logimove_Dll                     */
/*                                                          */
/*                                                          */
/*                                                          */
/*                                                          */
/*                                                          */
/* Autor(en)    : Ries Stefan                               */
/*                Sailer Andreas                            */
/* In Klasse transferiert von Hartl Florian					*/
/*															*/
/* Firma        : Hochschule Amberg-Weiden                  */
/* Abteilung    : EI / Software-Projektelabor               */
/*                                                          */
/* Stand        : 07. April 2009                            */
/*                                                          */
/* Historie     : 16.05.2006	erstellt                    */
/*                07.04.2009	erweitert um die Unter-     */
/*                              stuetzung der Logitech      */
/*                              Sphere AF                   */
/*				  02.01.2014 hf	In Klasse eingebettet		*/
/*				  06.01.2014 hf Bresenham-Algorithmus f�r	*/
/*								Kamerabewegung				*/
/*				  08.01.2014 hf Aufnahme und Senden eines	*/
/*								Bildes						*/
/*                                                          */
/* Copyright 2006 Josef P�sl, HAW Amberg-Weiden             */
/*                                                          */
/************************************************************/

#include "Cam.h"
#include <iostream>
using namespace std;


//Hilfsfunktion
/* signum function */
int sgn(double x)
{
  return (x > 0) ? 1 : (x < 0) ? -1 : 0;
}

//Einzelner Schritt nach Links
void Cam::stepleft()
{
	do						//solange versuchen bis die Kamera sich auch bewegen konnte.
	{
		this->Result = LGM_MoveLeft(this->Device);
	}while(this->Result == CAM_ERR_IN_USE);	

	if(this->Result != 0)
	{
		this->Result = GetLastError();
		if(this->Result == CAM_ERR_IN_USE)
		{
			printf("The Device is allready in use.\n");
		}
		else{
			/*
			code to deal with error-report (result not equal to 0)
			*/
		}
	}
	return;
}

//Schritt nach Rechts
void Cam::stepright()
{
	do						//solange versuchen bis die Kamera sich auch bewegen konnte.
	{
		this->Result = LGM_MoveRight(this->Device);
	}while(this->Result == CAM_ERR_IN_USE);	


	if(this->Result != 0)
	{
		this->Result = GetLastError();
		if(this->Result == CAM_ERR_IN_USE)
		{
			printf("The Device is allready in use.\n");
		}
		else{
			/*
			code to deal with error-report (result not equal to 0)
			*/
		}
	}
	return;
}

//Schritt nach Oben
void Cam::stepup()
{
	do						//solange versuchen bis die Kamera sich auch bewegen konnte. 
	{
		this->Result = LGM_MoveDown(this->Device);		//UP DOWN bei .dll verstausch!
	}while(this->Result == CAM_ERR_IN_USE);	

	if(this->Result != 0)
	{
		this->Result = GetLastError();
		if(this->Result == CAM_ERR_IN_USE)
		{
			printf("The Device is allready in use.\n");
		}
		else{
			/*
			code to deal with error-report (result not equal to 0)
			*/
		}
	}

	return;
}

//Schritt nach Unten
void Cam::stepdown()
{
	do						//solange versuchen bis die Kamera sich auch bewegen konnte.
	{
		this->Result = LGM_MoveUp(this->Device);	//UP DOWN bei .dll verstausch!
	}while(this->Result == CAM_ERR_IN_USE);	


	if(this->Result != 0)
	{
		this->Result = GetLastError();
		if(this->Result == CAM_ERR_IN_USE)
		{
			printf("The Device is allready in use.\n");
		}
		else{
			/*
			code to deal with error-report (result not equal to 0)
			*/
		}
	}

	return;
}

void Cam::image()
	//Zum Testen des bmp Einlesevorgangs wird eine Kopie des Bildes erzeugt. Stimmen die Bilder �berein war das einlesen erfolgreich. TODO: Wieder entfernen
	//Erkenntnis: Info-Header der Datei muss ge�ndert werden, damit es klappt!
{
	//unsigned char* bitmap = NULL;

	cout << "Versuche Bild aufzunehmen" << endl;

#ifdef WITHCAM


	CoInitialize(NULL);
	{
		// And take some images...
		this->hr = this->grabDevice->GrabImage(&(this->img));
		HBITMAP hBmp = NULL;

		cout << "Bild wird gespeichert" << endl;
		this->hr = this->img->CreateHBitmap(&hBmp);
		this->hr = this->img->Save(_bstr_t("img.bmp"));
		this->img = NULL;
		
	}
	CoUninitialize();

#endif


	cout << "Bild wird nochmals geoeffnet und in uchar-string gepackt" << endl;
	//�ffnen einer BMP Datei nach: http://cboard.cprogramming.com/c-programming/99714-open-read-bmp-image-files.html
    FILE *ptrBmpFileIn;		//File handles

    memset(this->header,0,54);			//Setzen aller Werte des Header-Arrays auf 0
    memset(this->data,0,480*640*3);		//Setzen aller Werte des Daten-Arrays auf 0
	   
    BITMAPFILEHEADER bMapFileHeader;		//Handle f�r Bitmap File Header
    BITMAPINFOHEADER bMapInfoHeader;		//Handle f�r Bitmap Info Header

    ptrBmpFileIn = fopen("img.bmp","rb");	//�ffnen der bmp Datei
             
    fseek(ptrBmpFileIn,0,SEEK_SET);			//Setzen des "Cursors" auf den Anfang des eingelesenen (Datei-)Streams          
    fread(&bMapFileHeader,sizeof(BITMAPFILEHEADER),1,ptrBmpFileIn);	//Lesen des File Headers
	fread(&bMapInfoHeader,sizeof(BITMAPINFOHEADER),1,ptrBmpFileIn);	//Lesen des Info Headers
    
	//Daran lags doch ;) 
	//Info Header Bearbeiten, da (falsche?) Werte
	bMapInfoHeader.biSizeImage = 480 * 640 * 3;
      
	fseek(ptrBmpFileIn,0,SEEK_SET);			//Setzen des "Cursors" auf den Anfang der Datei
	fread(this->header,1,sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER),ptrBmpFileIn);		//Einlesen des Headers

	fseek(ptrBmpFileIn,54,SEEK_SET);		//Setzen des "Cursors" auf den Anfang der Bilddaten: Offset 54
    fread(this->data,bMapInfoHeader.biSizeImage,1,ptrBmpFileIn);		//Einlesen der Bilddateien in den Daten-Array
	


	fclose(ptrBmpFileIn);


	return; 
}


void Cam::move(double xend, double yend)
{
	//Bewegung durch Bresenham-Algorithmus (Quelle: http://de.wikipedia.org/wiki/Bresenham-Algorithmus angepasst an diesen Fall bzw. Koordsys.)

	cout << "Bewege Kamera in..." << endl;
	cout << "x-Richtung (links/rechts) auf Pos: " << xend << endl;
	cout << "y-Richtung (oben/unten) auf Pos: " << yend << endl;

	double xinc;	//x-Weite in einenm Schritt (+ - 1/32)
	double yinc;	//y-Weite in einenm Schritt (+ - 1/32)

	double x;		//neue x-Position nach einem Schritt
	double y;		//neue y-Position nach einem Schritt

	double tol = 1./160;		//toleranz f�r Rundungsfehler 10%

	double pdx;		//x-Weite in einem Schritt bei Parallelschritt
	double pdy;		//y-Weite in einem Schritt bei Parallelschritt

	double ddx;		//x-Weite in einem Schritt bei Diagonalschritt
	double ddy;		//y-Weite in einem Schritt bei Diagonalschritt

	double dx, dy;	//Endposition - Startposition

	double el;		//Fehlerschritt bei langsamer Richtung
	double es;		//Fehlerschritt bei schneller Richtung
	double err;
	
	double t;		//Schleifenvariable
	int z=0;		//Z�hler TODO: f�r Debug wieder l�schen

	//Gesamtentfernung in beide Richtungen bestimmen:
	dx = xend - this->actXPos;
	dy = yend - this->actYPos;

	//Bewegungsrichtung durch Vorzeichen bestimmen
	xinc = sgn(dx) * 1./16;
	yinc = sgn(dy) * 1./16;

	//Absolutbetrag der Entfernungsdifferenz bilden
	if(dx<0) dx = -dx;
    if(dy<0) dy = -dy;

	//feststellen welche Entfernung gr��er ist. Festlegen der "schnellen" und "langsamen" Richtung
	if(dx > dy)
	{
		//x ist schnelle Richtung

		//f�r Fall 1 Parallelschritt
		pdx = xinc;
		pdy = 0;		//keine Bewegung in langsame y Richtung bei Parallelschritt!

		//fur Fall 2 Diagonalschritt
		ddx = xinc;		
		ddy = yinc;

		//Fehlerschritte f�r schnell/langsam
		es = dy;
		el = dx;

	}
	else
	{
		//y ist schnelle Richtung

		//f�r Fall 1 Parallelschritt
		pdx = 0;		//keine Bewegung in langsame x Richtung bei Parallelschritt!
		pdy = yinc;		

		//fur Fall 2 Diagonalschritt
		ddx = xinc;		
		ddy = yinc;

		//Fehlerschritte f�r schnell/langsam
		es = dx;
		el = dy;
	}

	//Startwerte
	err = el/2;
	x = this->actXPos;
	y = this->actYPos;

	for(t = 0; t < el; t= 1./16 + t)
	{
		cout << "Schritt : " << ++z;

		err -= es;
		if(err < 0)
		{
			err += el;
			//Diagonalschritt
			x += ddx;
			y += ddy;
		}
		else
		{
			//Parallelschritt
			x += pdx;
			y += pdy;
		}

		if(x < this->actXPos + tol && x > this->actXPos - tol)	//keine Bewegung Links/Rechts
		{
			
		}
		else if(x > this->actXPos)	//Bewege nach Links
		{	
			cout << "Bewege Kamera nach links" << endl;
			//Doppelschritt, da Aufl�sung der Bewegung links/rechts doppelt so hoch wie nach oben/unten ist!
			this->stepleft();
			this->stepleft();

			this->actXPos += (1./16);	//Aktuelle Position um einen Schritt weiter setzen (32 Schritte vom Zentrum aus zum Rand)
			
		}
		else					//Bewege nach Rechts
		{	
			cout << "Bewege Kamera nach Rechts" << endl;
			//Doppelschritt, da Aufl�sung der Bewegung links/rechts doppelt so hoch wie nach oben/unten ist!
			this->stepright();
			this->stepright();

			this->actXPos -= (1./16);	//Aktuelle Position um einen Schritt weiter setzen (32 Schritte vom Zentrum aus zum Rand)
		}

		if(y < this->actYPos + tol && y > this->actYPos - tol)	//keine Bewegung Oben/Unten
		{
			
		}
		else if(y > this->actYPos)	//Bewege nach Oben
		{	
			cout << "Bewege Kamera nach oben" << endl;
			this->stepup();
			this->actYPos += (1./16);	//Aktuelle Position um einen Schritt weiter setzen (32 Schritte vom Zentrum aus zum Rand)
			
		}
		else					//Bewege nach Unten
		{	
			cout << "Bewege Kamera nach Unten" << endl;
   
			this->stepdown();

			this->actYPos -= (1./16);	//Aktuelle Position um einen Schritt weiter setzen (32 Schritte vom Zentrum aus zum Rand)
		}

		//weiter mit n�chstem Schritt (Parallel oder Diagonal)
		cout << "Aktuelle Postion" << endl;
		cout << "x: " << this->actXPos << endl;
		cout << "y: " << this->actYPos << endl;
	}

	return;
}

//Konstruktor der Klasse initialisiert auch die Hardware der Kameraansteuerung und f�hrt Kamera in Center-Position
Cam::Cam(void)
{
	this->Device = INVALID_HANDLE_VALUE;
	this->BufferSize = 0;
	this->input_error = 0;
	this->devnumber = 0;
	this->connected_Devices = 0;
	this->ptr_Device = &(this->Device);
	this->devString = NULL;

	memset(this->header,0,54);
	memset(this->data,0,480*640*3);

#ifdef WITHCAM
	while(this->Device == INVALID_HANDLE_VALUE)	//Versuche mit Kamera zu verbinden bis es klappt ;)
	{
		this->Result = LGM_GetDeviceCount(&(this->connected_Devices));
		//TODO: code to deal with error-report (result not equal to 0)

		if(this->connected_Devices > 0)
		{
			this->Result = LGM_OpenDevice(1, this->ptr_Device);		//erste Kamera wird benutzt, egal wie viele kameras angeschlossen werden
			this->devnumber = 1;
		}
		else
		{
			printf("No camera detected. Please (re)connect camera\n");
			system("pause");
			this->Device = INVALID_HANDLE_VALUE;
		}
	}
	cout << "Kamera erfolgreich verbunden" << endl;

	this->Result = LGM_MoveHome(this->Device);
	if(Result != 0)
	{
		this->Result = GetLastError();
		if(this->Result == CAM_ERR_IN_USE)
		{
			cout << "Kamera bereits in Benutzung" << endl;

		}
		else
		{
			cout << "Sonstiger Fehler mit Kamera" << endl;
			//TODO: weitere Fehlerbehandlung
		}
	}
	else
	{
		cout << "Kamera erfolgreich in Home-Position" << endl;
	}


	//CoInitialize(NULL);
	//{
		
		this->hr = IMCCreateEnumGrabDevices(&(this->grabDevices));
		if (this->hr != S_OK)
		{
			cout << "No devices found" << endl;
			CoUninitialize();
			return;
		}

		

		// List all devices
		while(this->grabDevices->Next(1, &(this->grabDevice), NULL) == S_OK)
		{
			BSTR devNameTmp;
			this->grabDevice->GetName(&devNameTmp);
			_bstr_t devName(devNameTmp, FALSE);
			cout << devName << endl;
			this->grabDevice = NULL;
		}
		// Get the first device
		this->grabDevices->Reset();
		this->hr = this->grabDevices->Next(1, &(this->grabDevice), NULL);

	//}
	//CoUninitialize();

#endif

	this->actXPos = 0;
	this->actYPos = 0;

}


Cam::~Cam(void)
{
}
