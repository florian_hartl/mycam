/********************************************************************/
/*																	*/
/* Inhalt:    Implementierung der Benutzerklasse "User"				*/
/*																	*/
/* Autor(en): Hartl Florian (hf)									*/
/* Firma:     Ostbayerische Technische Hochschule Amberg-Weiden		*/
/* Stand:     18.12.2013											*/
/*																	*/
/* Historie:  17.12.2013 hf  erstellt								*/
/*            18.12.2013 hf	 Dekonstruktor erweitert				*/
/*			  19.12.2013 hf		umstellung von char* auf string		*/
/*			  28.12.2013 hf		erweiterung der Methoden     		*/
/*																	*/
/* Copyright 2013-2050 Hartl Florian								*/
/*																	*/
/********************************************************************/

#include "User.h"

#include <iostream>
using namespace std;

#include <time.h>

#include "md5.cpp"			//md5 Hash-Funktion


//-----------------------------------------------------------------------------
//Setzen des Passworts
//-----------------------------------------------------------------------------
void User::SetPassword(string newpassword)
{
	
	size_t index = 0;
	index = newpassword.find(";");
	if(index <= newpassword.length())
		cout << "Ungueltiges Zeichen ; im Passwort!" << endl;
	else
	{
		this->SetLastaction();
		this->password = newpassword;
	}
}

//-----------------------------------------------------------------------------
//Setzen der ID
//-----------------------------------------------------------------------------
void User::SetID(long newID)
{
	this->ID = newID;
}

long User::randomID()
{
	unsigned int seed = 0;		//Seed der randfunktion
	time_t actTime = 0;			//aktuelle Systemzeit

	//Seed mit Systemzeit f�ttern f�r bessere Zufallszahlen
	seed = (unsigned int) time(&actTime);	
	srand(seed);

	//F�r ID wird zuf�llig oft "gew�rfelt" (max. 100 mal min. 1 mal)
	for(int i=seed%100; i>=0; i--)
	{
		ID = (long) rand();
	}

	this->SetID(ID);
	return ID;
}

//-----------------------------------------------------------------------------
//Setzen des n�chsten Users (verketten der Liste)
//-----------------------------------------------------------------------------
void User::SetNextUser(User* nextUser)
{
	this->nextUser = nextUser;
}

//-----------------------------------------------------------------------------
//Aktualisieren des Zeitstempels f�r die letzte Aktion mit aktueller Systemzeit
//-----------------------------------------------------------------------------
void User::SetLastaction()
{
	time(&lastaction);
}

//-----------------------------------------------------------------------------
//Pr�fen auf User TimeOut
//-----------------------------------------------------------------------------
bool User::checkTimeOut()
{
	time_t actTime;
	time(&actTime);		//aktuelle Zeit einlesen

	if(actTime > TIMEOUT + this->lastaction)		//ist die letzte Aktion zu lange her, wird ein Timeout ausgel�st
	{
		return false;
	}
	else
	{
		return true;
	}
}

//-----------------------------------------------------------------------------
//Konstruktor des Users mit Angabe des Usernamen und -Passworts
//-----------------------------------------------------------------------------
User::User(string name, string pw)
{
	size_t index = 0;

	this->ID = this->randomID();		//Zuf�llige ID vergeben als Startwert (kann nicht einfach eraten werden wie 0);				//Nullsetzen der ID und Zeiger auf n�chsten User (default ung�ltige Werte)
	this->nextUser = NULL;
	this->lastaction = 0;

		
	index = name.find(";");		//suchen des ung�ltigen Zeichens im string
	if(index <= name.length())
	{
		cout << "Ungueltiges Zeichen ; im Usernamen!" << endl;
		//TODO: weitere Fehlerbehandlung
	}
	else
		this->name = name;		//kopieren des Usernamens in dieses Objekt

	index = pw.find(";");		//suchen des ung�ltigen Zeichens im string
	if(index <= pw.length())
	{
		cout << "Ungueltiges Zeichen ; im Passwort!" << endl;
		//TODO: weitere Fehlerbehandlung
	}
	else
		this->password = pw;		//kopieren des Passworts in dieses Objekt

		


}

//-----------------------------------------------------------------------------
//Dekonstruktor des Users
//-----------------------------------------------------------------------------
User::~User(void)
{
	cout << "Dekonstruktor User" << endl;	//TODO: wieder l�schen

	if(this->nextUser != NULL)
		delete nextUser;
	
}
