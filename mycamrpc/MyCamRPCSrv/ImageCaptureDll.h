/************************************************************/
/*                                                          */
/* Inhalt:    DLL-Funktionen f�r Bildaufnahme(-ger�te)      */
/*                                                          */
/* Autor(en): Josef P�sl (jp), <XXX>                        */
/* Firma:     Fachhochschule Amberg-Weiden                  */
/* Stand:     06. Jan 2005                                  */
/*                                                          */
/* Historie:  06. Jan 2005 jp  erstellt                     */
/*            xx. xxx xxxx     modifiziert...               */
/*                                                          */
/* Copyright 2001-2050 FH Amberg-Weiden ... usw.            */
/*                                                          */
/************************************************************/

#ifndef IMAGECAPTUREDLL_H
#define IMAGECAPTUREDLL_H

#include "ImageCapture.h"

#ifdef IMAGECAPTUREDLL_EXPORTS
#define IMAGECAPTUREDLL_API __declspec(dllexport)
#else
#define IMAGECAPTUREDLL_API __declspec(dllimport)
#endif

IMAGECAPTUREDLL_API HRESULT IMCCreateEnumGrabDevices(IEnumGrabDevices **enumGd);

#endif  // IMAGECAPTUREDLL_H
