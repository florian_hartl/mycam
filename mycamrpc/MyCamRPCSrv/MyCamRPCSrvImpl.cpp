/********************************************************************/
/*																	*/
/* Inhalt:    Implementierung der RPC-Funktionen					*/
/*																	*/
/* Autor(en): Hartl Florian (hf)									*/
/* Firma:     Ostbayerische Technische Hochschule Amberg-Weiden		*/
/* Stand:     29.12.2013											*/
/*																	*/
/* Historie:  13.12.2013 hf  erstellt								*/
/*			  28.12.2013 hf  erweitern um weitere Funktionen		*/
/*																	*/
/* Copyright 2013-2050 Hartl Florian								*/
/*																	*/
/********************************************************************/

#include "MyCamRPC_i.h"
#include "RpcException.h"

#include "Usermanager.h"
#include <iostream>
using namespace std;


//-----------------------------------------------------------------------------
//Vorwärtsdeklaration von Funktionen
//-----------------------------------------------------------------------------
string uchar_to_str(unsigned char* ucInput);

//-----------------------------------------------------------------------------
//Implementierung der RPC Funktionen
//-----------------------------------------------------------------------------

//Testfunktion
void test(long x, long *y)
{
	*y = 2*x;
	return;
}

//user-login
long login(unsigned char* name, unsigned char* pw)
{
	Usermanager* usermanager = Usermanager::GetInstance();
	long ID = 0;
	
	string s_name;
	string s_pw;
	

	//Umwandeln der unsigned char-strings zu je einem string
	s_name = uchar_to_str(name);
	s_pw = uchar_to_str(pw);


	ID = usermanager->login(s_name,s_pw);

	return ID;
}



//Ändern des Passworts
RPC_STATUS changepw(long ID, unsigned char* newpw)
{
	Usermanager* usermanager = Usermanager::GetInstance();
	User* pUser = usermanager->checkID(ID);
	string s_newpw;

	if(pUser->checkTimeOut() == false)
	{
		cout << "Timeout" << endl;	//TODO: weitere Fehlerbehebung -> Rückmeldung an User -> neuanmeldung
		return RPC_S_TIMEOUT;
	}

	cout << "User will PW aendern" << endl;
	s_newpw = uchar_to_str(newpw);	//Umwandeln in string
	pUser->SetPassword(s_newpw);	//Setzen des neuen Passworts

	cout << "neues passwort: " << pUser->GetPassword() << endl;
	usermanager->save();			//persistentes Speichern des neuen Passworts

	return RPC_S_OK;
}

RPC_STATUS move(long ID, double x, double y)
{
	Usermanager* usermanager = Usermanager::GetInstance();
	User* pUser = usermanager->checkID(ID);

	if(pUser != NULL)
	{
		if(pUser->checkTimeOut() == false)
		{
			cout << "Timeout" << endl;
			return RPC_S_TIMEOUT;
		}
		pUser->SetLastaction();					//Letzte Aktivität aktualisieren
		usermanager->GetCam()->move(x, y);
		return (RPC_STATUS) usermanager->GetCam()->GetResult();
	}
	else
	{
		cout << "keine gueltige Sitzungs-ID" << endl;
		return ERR_BADID;
	}
}

RPC_STATUS takeimage(long ID,long headersize, long datasize, unsigned char* header, unsigned char* data)
{
	Usermanager* usermanager = Usermanager::GetInstance();
	User* pUser = usermanager->checkID(ID);


	if(pUser != NULL)
	{
		if(pUser->checkTimeOut() == false)
		{
			cout << "Timeout" << endl;	//TODO: weitere Fehlerbehebung -> Rückmeldung an User -> neuanmeldung
			return RPC_S_TIMEOUT;
		}

		pUser->SetLastaction();
		usermanager->GetCam()->image();
		//memcpy(bitmap,usermanager->GetCam()->header,54);
		//memcpy(bitmap+54,usermanager->GetCam()->data,480*640*3);

		if(usermanager->GetCam()->GetResult() == 0)
		{
			memcpy(header, usermanager->GetCam()->header, 54);
			memcpy(data, usermanager->GetCam()->data, 480*640*3);
			return RPC_S_OK;
		}
		else if(usermanager->GetCam()->GetResult() == CAM_ERR_IN_USE)
		{
			return CAM_ERR_IN_USE;
		}
		else
		{
			return CAM_ERR;
		}

	}
	else
	{
		cout << "keine gueltige Sitzungs-ID" << endl;
		return ERR_BADID;
	}
}



//Server Beenden nur für Debugzwecke TODO: Aus RPC Schnittstelle löschen!
RPC_STATUS ShutdownSrv()
{

	RPC_STATUS rpcStatus;

	try
	{
	// Warten abbrechen
	rpcStatus = RpcMgmtStopServerListening(NULL);
	if(rpcStatus == RPC_S_OK)
		cout << "RpcMgmtStopServerListening erfolgreich" << endl;
	else
		throw(RpcException(rpcStatus,(string)"RpcMgmtStopServerListening failed", "RPC Runtime Error"));

	// Schnittstelle deregistrieren
	rpcStatus = RpcServerUnregisterIf(NULL, NULL, FALSE);
   	if(rpcStatus == RPC_S_OK)
		cout << "RpcServerUnregisterIf erfolgreich" << endl;
	else
		throw(RpcException(rpcStatus,(string)"RpcServerUnregisterIf failed", "RPC Runtime Error"));
	}
	catch(RpcException &e)
	{
		cout << e.GetErrorText() << endl;

	}

	return rpcStatus;
}

//-----------------------------------------------------------------------------
//Zusätzliche Hilfsfunktionen
//-----------------------------------------------------------------------------

//Umwandlung eines unsigned char strings zu einem string
string uchar_to_str(unsigned char* ucInput)
{
	string sOutput;
	int i;
	for(i=0; ucInput[i] != '\0' ; i++)
	{
		sOutput += ucInput[i];
	}
	return sOutput;
}





