/********************************************************************/
/*																	*/
/* Inhalt:    RPC Server f�r Webcam-Steuerung						*/
/*																	*/
/* Autor(en): Hartl Florian (hf)									*/
/* Firma:     Ostbayerische Technische Hochschule Amberg-Weiden		*/
/* Stand:     18.12.2013											*/
/*																	*/
/* Historie:  13.12.2013 hf  erstellt								*/
/*																	*/
/* Copyright 2013-2050 Hartl Florian								*/
/*																	*/
/********************************************************************/

#include <iostream>
#include <exception>
#include "MyCamRPC_i.h"
#include "MyCamRPCConfig.h"
#include "RpcException.h"
#include "Usermanager.h"


		
//#include "md5.cpp"			//md5 Hash-Funktion TODO: hier n�tig???

using namespace std;

//statische Attribute m�ssen hier(?) definiert werden
Usermanager* Usermanager::singleton_instance = NULL;	
int Usermanager::instanceCounter = 0;
CRITICAL_SECTION Usermanager::CS;

int main(void)
{
//-----------------------------------------------------------------------------
//Variablendeklaration
//-----------------------------------------------------------------------------

			CoInitialize(NULL);
		{

	RPC_STATUS rpcStatus = 0;							//R�ckgabewert der RPC-Funtionen -> Fehlerbehandlung

	char* str_RPC_ENDPOINT = (char*)RPC_ENDPOINT;		//Erstellen eines strings aus Port-Nr zur Ausgabe

	Usermanager* Usermng = Usermanager::GetInstance();

	if(Usermng->GetFirstUser() == NULL)
	{
		cout << "Fehler beim Laden der User-Liste (Liste leer?)" << endl;
		return -1;
	}

//-----------------------------------------------------------------------------
//RPC-Server initialisieren
//-----------------------------------------------------------------------------

	//TESTS TODO: wieder l�schen

	//Usermng->GetCam()->move(0, 1);
	//Usermng->GetCam()->move(0, -1);
	//Usermng->GetCam()->move(-1, -1);
	//Usermng->GetCam()->move(1, -1);
	//Usermng->GetCam()->move(-1, 1);
	


	try
	{
		// Protokoll und Endpunkt registrieren
		rpcStatus = RpcServerUseProtseqEp((UCHAR*)RPC_PROT_SEQ, SRV_MAX_CALLS, (UCHAR*)RPC_ENDPOINT, NULL);
		if(rpcStatus == RPC_S_OK)
			cout << "Protokoll und Endpunkt wurden registriert" << endl;
		else
		{
			throw(RpcException(rpcStatus,(string)"RpcServerUseProtseqEp (Protokoll und Endpunkt registrieren) failed \nPort Nr = "+ str_RPC_ENDPOINT, "RPC Runtime Error"));
		}
		
		// Schnittstelle registrieren
		rpcStatus = RpcServerRegisterIf(MyCamRPC_i_v1_0_s_ifspec, NULL, NULL);

		if(rpcStatus == RPC_S_OK)
			cout << "Schnittstelle wurde registriert" << endl;
		else
			throw(RpcException(rpcStatus,(string)"RpcServerRegisterIf (Schnittstelle registrieren) failed", "RPC Runtime Error"));
	
//-----------------------------------------------------------------------------
// Auf Client Anforderungen warten
//-----------------------------------------------------------------------------



		cout << "Warte auf Client-Aufrufe" << endl;
		rpcStatus = RpcServerListen(SRV_MIN_CALLS, SRV_MAX_CALLS, FALSE);
		
		if(rpcStatus == RPC_S_OK)
			cout << "RpcServerListen erfolgreich beendet" << endl;
		else
			throw(RpcException(rpcStatus,(string)"RpcServerListen failed", "RPC Runtime Error"));


	}
//-----------------------------------------------------------------------------
//Fehlerbehandlung bei RPC-Exceptions
//-----------------------------------------------------------------------------

	catch(RpcException &e)
	{
		cout << e.GetErrorText() << endl;
		cout << "Fehler Nr: " << e.GetStatus() << endl;
	}

	}
	CoUninitialize();

	system("pause");
	return 0;
}
