/********************************************************************/
/*																	*/
/* Inhalt:    Benutzerklasse "User"									*/
/*																	*/
/* Autor(en): Hartl Florian (hf)									*/
/* Firma:     Ostbayerische Technische Hochschule Amberg-Weiden		*/
/* Stand:     18.12.2013											*/
/*																	*/
/* Historie:  17.12.2013 hf  erstellt								*/
/*			  19.12.2013 hf		umstellung von char* auf string		*/
/*																	*/
/* Copyright 2013-2050 Hartl Florian								*/
/*																	*/
/********************************************************************/

#pragma once
#define _CRT_SECURE_NO_WARNINGS		//Deaktivieren der Warnung bei fopen etc.
#include <string>
#include "MyCamRPCConfig.h"

class User 
{
private:
	std::string name;			//Name des Users
	std::string password;		//Passwort des Users
	long ID;					//Sitzungs-ID
	time_t lastaction;		

	User* nextUser;



public:
	User(std::string name, std::string pw);
	~User(void);

	std::string GetName() {return this->name;};
	std::string GetPassword() {return this->password;};
	long GetID() {return this->ID;};
	User* GetNextUser() {return this->nextUser;};
	time_t GetLastaction() {return this->lastaction;};


	void SetPassword(std::string newpassword);
	void SetID(long ID);
	long randomID();			//Erzeugen einer zufälligen ID
	void SetNextUser(User* nextUser);
	
	void SetLastaction();
	bool checkTimeOut();



};

