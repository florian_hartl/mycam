/************************************************************/
/*                                                          */
/* Inhalt:    LOGIMOVE_DLL zur Steuerung einer Logitech     */
/*            Quick Cam Sphere                              */
/*                                                          */
/*                                                          */
/*                                                          */
/*                                                          */
/* Autor(en)    : Ries Stefan                               */
/*                Sailer Andreas                            */
/* Firma        : Hochschule Amberg-Weiden                  */
/* Abteilung    : EI / Software-Projektelabor               */
/*                                                          */
/* Stand        : 07. April 2009                            */
/*                                                          */
/* Historie     : 16.05.2006	erstellt                    */
/*                07.04.2009	erweitert um die Unter-     */
/*                              stuetzung der Logitech      */
/*                              Sphere AF                   */
/*                                                          */
/* Copyright 2006 Josef P�sl, HAW Amberg-Weiden             */
/*                                                          */
/************************************************************/

#ifndef LOGIMOVE_DLL_H
#define LOGIMOVE_DLL_H

#ifdef LOGIMOVE_DLL_EXPORTS
#define LOGIMOVE_DLL_API __declspec(dllexport)
#else
#define LOGIMOVE_DLL_API __declspec(dllimport)
#endif

#include <windows.h>


LOGIMOVE_DLL_API HRESULT LGM_GetDeviceCount(int* connected_Devices);
LOGIMOVE_DLL_API HRESULT LGM_OpenDevice(int devInd, HANDLE* BufferForHandle);
LOGIMOVE_DLL_API HRESULT LGM_GetDevicePath(int devInd, char *devPath, int *devPathMaxLen);
LOGIMOVE_DLL_API HRESULT LGM_GetDeviceName(int devInd, char *devName, int *devPathMaxLen);

LOGIMOVE_DLL_API HRESULT LGM_MoveHome(HANDLE Device_Handle);
LOGIMOVE_DLL_API HRESULT LGM_MoveLeft(HANDLE Device_Handle);
LOGIMOVE_DLL_API HRESULT LGM_MoveRight(HANDLE Device_Handle);
LOGIMOVE_DLL_API HRESULT LGM_MoveUp(HANDLE Device_Handle);
LOGIMOVE_DLL_API HRESULT LGM_MoveDown(HANDLE Device_Handle);
LOGIMOVE_DLL_API HRESULT LGM_MoveCompleteLeft(HANDLE Device_Handle);
LOGIMOVE_DLL_API HRESULT LGM_MoveCompleteRight(HANDLE Device_Handle);
LOGIMOVE_DLL_API HRESULT LGM_MoveCompleteUp(HANDLE Device_Handle);
LOGIMOVE_DLL_API HRESULT LGM_MoveCompleteDown(HANDLE Device_Handle);

LOGIMOVE_DLL_API HRESULT LGM_CloseHandle(HANDLE* devHandle);

#endif  // LOGIMOVE_DLL_H