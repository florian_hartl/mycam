/********************************************************************/
/*																	*/
/* Inhalt:    Konstantendefinition f�r gesamtes Projekt 			*/
/*																	*/
/* Autor(en): Hartl Florian (hf)									*/
/* Firma:     Ostbayerische Technische Hochschule Amberg-Weiden		*/
/* Stand:     29.12.2013											*/
/*																	*/
/* Historie:  15.12.2013 hf  erstellt								*/
/*																	*/
/* Copyright 2013-2050 Hartl Florian								*/
/*																	*/
/********************************************************************/

#ifndef MyCamRPCConfig_H
#define MyCamRPCConfig_H

#define RPC_PROT_SEQ		"ncacn_ip_tcp"
#define RPC_ENDPOINT		"1200"
#define RPC_SRV_NETWADDR	"127.0.0.1"

#define SRV_MAX_CALLS		10
#define SRV_MIN_CALLS		1

#define LOGIN_ERR_WRONGPW -1		//Fehler bei falschem login-Passwort
#define LOGIN_ERR_NOUSER -2			//Fehler bei nicht gefundenem User-Namen

#define ERR_BADID -3				//Fehler bei nicht g�ltiger Sitzungs-ID

#define CAM_ERR_IN_USE 170
#define CAM_ERR 42				//sonstiger Kamera-Fehler

#define TIMEOUT 300				//Timeout in Sekunden (=5min), bei der der User automatisch ausgeloggt wird

#define WITHCAM					//Schalter um auch mit nicht angeschlossener Kamera zu programmieren TODO: im release entfernen



#endif